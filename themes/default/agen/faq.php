<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?= base_url('assets/uploads/avatars/nineswidosari.png') ?>">
<style type="text/css">

  img.faqimg{display: block; margin: auto;}
.fixed-bg-faq {
  background-image: url("<?= base_url('styleFAQ/image/faq3.png') ?>");
  width: 100%;
  background-position: center;
  background-repeat: no-repeat;
  /*background-size: cover;*/
}
@media only screen and (max-width: 700px) {
  img.faqimg{width: 70%; margin: auto;}
.fixed-bg-faq {
  background-image: url("<?= base_url('styleFAQ/image/faq4.png') ?>");
  width: 100%;
  background-position: center;
  background-repeat: no-repeat;
  /*background-size: cover;*/
}
}
@media only screen and (max-width: 500px) {
  img.faqimg{width: 67%;margin: auto;}
}
</style>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?= base_url() ?>styleFAQ/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?= base_url() ?>styleFAQ/css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?= base_url() ?>styleFAQ/js/jquery_mobile/jquery.mobile.custom.theme.min.css">
	<title>FAQ | Reseller Online</title>
</head>
<body>
<header class="fixed-bg-faq">
</header>
<section class="cd-faq">
	<ul class="cd-faq-categories">
		<li><a class="selected" href="#question_1">Reseller Baru</a></li>
		<li><a href="#question_2">Fitur & Fungsi</a></li>
	</ul> <!-- cd-faq-categories -->

	<div class="cd-faq-items">
		<!-- Accordion nya UL -->
		<ul id="question_1" class="cd-faq-group">
			<!-- Judul -->
			<li class="cd-faq-title"><h2>Reseller Baru</h2></li>
			<!-- pertanyaan~nya <li> -->
			<li>
				<a class="cd-faq-trigger" href="#0">#1 Bagaimana mendaftarkan diri menjadi Reseller Online ?</a>
				<div class="cd-faq-content">
					<p>Pihak kami telah menyediakan Form Pendaftaran melalui <a href="http://bit.ly/FormResellerRefanes" target="_blank">http://bit.ly/FormResellerRefanes</a> atau <a href="https://api.whatsapp.com/send?phone=6282128524343&text=Assalamualaikum%20Admin%20Saya%20Mau%20Daftar%20Reseller" target="_blank">Via Whatsapp</a>.<br>
						Anda akan mendapatkan konfirmasi di E-mail mengenai <q>Isi Password</q> dan anda otomatis telah menjadi Reseller Online</p>
				</div>
			</li>
<!-- 			<li>
				<a class="cd-faq-trigger" href="#0">#2 Mengapa saat mendaftar harus menggunakan account G-Mail</a>
				<div class="cd-faq-content">
					<p>Jawabannya #1-2</p>
				</div>
			</li> -->
			<li>
				<a class="cd-faq-trigger" href="#0">#2 Apa yang harus saya lakukan bila <q>Lupa Password</q> ?</a>
				<div class="cd-faq-content">
					<p>Untuk menjaga keamanan beserta kenyamanan anda, kami telah menyediakan Link bila anda lupa password.<br>
					silakan kunjungi <a href="<?= base_url('onlinereseller'); ?>" target="_blank"><?= base_url(); ?></a> lalu klik forget password, terlihat seperti gambar dibawah ini.<p></p>
					<img src="<?= base_url('styleFAQ/image/forgot.png') ?>" class="faqimg"> 
					</p>
				</div>
			</li>
		</ul>


		<ul id="question_2" class="cd-faq-group">
			<!-- Judul -->
			<li class="cd-faq-title"><h2>Fitur & Fungsi</h2></li>
			<!-- pertanyaan~nya <li> -->
			
			<li>
				<a class="cd-faq-trigger" href="#0">#1 Bagaimana cara berjualan sebagai reseller refanes ?</a>
				<div class="cd-faq-content">
					<p>Pastikan kamu sudah terdaftar menjadi reseller
dan telah lengkap mengisi data diri serta kelengkapan
Foto KTP dan mendaftarkan No Rekening yang aktif<br><br>

- Reseller dapat menawarkan produk Refanes dengan
menggunakan Catalogue online melalui 
instagram @Refanes_catalogue
facebook @Refanes<br><br>

- Selain dari itu, Reseller juga dapat berjualan dengan cara yang kedua yaitu:
Reseller menyimpan gambar produk Refanes dari Facebook/instagram
lengkap dengan keterangan dan harga produk. Lalu Reseller dapat menyebar
penawaran produk melalui status WA, grup WA, status facebook, story facebook,
Feed dan story instagram, atau media sosial lainnya untuk mendapatkan konsumen<br><br>

- Setelah konsumen menentukan produk yang diminati,
maka Reseller meminta alamat lengkap konsumen sebagai tujuan pengiriman
barang disertai nomer HP konsumen.<br><br>

- Lalu Reseller menghubungi admin refanes melalui WA (whatsapp)
untuk konfirmasi pesanan dengan memberikan informasi dengan format:
Nama Produk_Size Produk_Jumlah Produk_Alamat pengiriman<br><br>

- Admin penjualan selanjutnya membuatkan invoice (tagihan) untuk Reseller 
yang berisikan nominal yang harus ditransfer oleh konsumen. Kemudian
reseller mengirimkan invoice tersebut kepada konsumen dan mengingatkan
kepada konsumen untuk melakukan nominal transfer sesuai dengan yang
tercantum pada invoice karena jumlah nominal 3 digit dibelakangnya 
merupakan Kode unik transaksi agar terdeteksi oleh sistem pembayaran kami.<br><br>

- Jika konsumen telah melakukan pembayaran, Pesanan akan segera dikirim.<br><br>

- Untuk membangun kepercayaan konsumen, Reseller
yang telah resmi terdaftar diperbolehkan mengubah nama akun
medsos dengan penambahan Reseller Refanes.
Seperti contoh: Nia Reseller Refanes, Nia Refanes, dsb</p>

lihat lebih detailnya di <a href="https://nineswidosari.com/panduan-berjualan-online-nw/">Panduan</a>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#2 Apa keuntungan menjadi reseller refanes ?</a>
				<div class="cd-faq-content">
					<p>Sangat menguntungkan, karena :<br>
						
							1. Tanpa perlu Modal awal<br>
							2. Punya penghasilan tanpa keluar rumah<br>
							3. Dapat komisi 10%<br>
							4. Tanpa perlu Survey lokasi<br>
							5. Tanpa menyiapkan tempat<br>
							6. Tanpa Stok Barang<br><br>
						yang paling utamanya adalah membagi peluang usaha untuk semua orang tanpa harus membebani.
					</p>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#3 Bagaimana cara Cek Saldo komisi hasil penjualan saya ?</a>
				<div class="cd-faq-content">
					<p>Anda bisa melihatnya setelah login di menu depan / dashboard disebelah kanan<br>Saldo akan muncul bila konsumen sudah membayar.
						<img src="<?= base_url('styleFAQ/image/look_saldo.png') ?>" class="faqimg"><br>
						 Saldo merupakan selisih antara Debit & Kredit, detailnya bisa anda lihat di menu <q>Fee & Payment</q>
					</p>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#4 Bagaimana prosedur pencairan dana komisi sebagai reseller refanes ?</a>
				<div class="cd-faq-content">
					<p>Pembayaran dilakukan setiap akhir bulan dan dikirimkan ke No Rekening anda, Jumlah pembayaran berasal dari saldo yang anda dapatkan.</p>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#5 Apa saya bisa melihat Stock barang ?</a>
				<div class="cd-faq-content">
					<p>Stock bisa anda lihat di menu <q>Check Stock</q> dengan mengisikan Nama produk / kode produk, maka system akan menampilkan stock masih ada atau tidak. Disediakan Pula menu <q>Gallery</q> agar anda bisa menjelajah tentang produk kami lalu menawarkan ke customer.</p>
				</div>
			</li>
		</ul>


<!-- 		<ul id="question_3" class="cd-faq-group">
			<li class="cd-faq-title"><h2>Question 3</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">#3 Pertanyaan yang sering diajukan</a>
				<div class="cd-faq-content">
					<p>Jawabannya #3</p>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#3-1 Pertanyaan yang sering diajukan</a>
				<div class="cd-faq-content">
					<p>Jawabannya #3-1</p>
				</div>
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">#3-2 Pertanyaan yang sering diajukan</a>
				<div class="cd-faq-content">
					<p>Jawabannya #3-2</p>
				</div>
			</li>
		</ul> -->

		

		
	</div> <!-- cd-faq-items -->
	<a href="#0" class="cd-close-panel">Close</a>
</section> <!-- cd-faq -->

<script src="<?= base_url() ?>styleFAQ/js/jquery.min.js"></script>
<script src="<?= base_url() ?>styleFAQ/js/modernizr.js"></script> <!-- Modernizr -->
<script src="<?= base_url() ?>styleFAQ/js/jquery_mobile/jquery.mobile.custom.min.js"></script>
<script src="<?= base_url() ?>styleFAQ/js/main.js"></script> <!-- Resource jQuery -->

</body>
</html>