
        <?php
        	$array = json_decode($data_omzet, true);
        	$arr_push1 = array(); // jumlah
        	$arr_push2 = array(); // nama
        	$arr_push3 = array(); // color
        	$arr_push4 = array(); // persen

        	foreach ($array['data'] as $key => $value) {
        		$x = $value['value'];
        		$y = $value['label'];
        		array_push($arr_push1, $x);
        		array_push($arr_push2, $y);
        	}
        	foreach ($array['colors'] as $key => $value) {
        		array_push($arr_push3, $value);
        	}
        		$jml = array_sum($arr_push1);

        	foreach ($array['data'] as $key => $value) {
				$x = ($value['value'] / ($jml/100));
				$persen = round($x)."%";
				array_push($arr_push4, $persen);
			}
        ?>
<div class="tab-pane show <?= $homes; ?>" id="tabs_1" role="tabpanel">
							<!-- BEGIN: Subheader -->
							<div class="m-subheader ">
								<div class="d-flex align-items-center">
									<div class="mr-auto">
										<h3 class="m-subheader__title ">Dashboard <?php echo "(#ID ".$this->session->userdata('companies_id').")"; ?></h3>
									</div>
								</div>
							</div>

							<!-- END: Subheader -->
							<div class="m-content">
								<!--Begin::Section-->
								<div class="m-portlet">
									<div class="m-portlet__body  m-portlet__body--no-padding">
										<div class="row m-row--no-padding m-row--col-separator-xl">


										<div class="col-xl-6">
												<!--begin:: Widgets/Daily Sales-->
												<div class="m-widget14">
													<div class="m-widget14__header m--margin-bottom-30">
														<h3 class="m-widget14__title">
															Daftar Penjualan Yang Belum Membayar
														</h3>
														<span class="m-widget14__desc">
															Detail daftar pembayaran (pending)
														</span>
													</div>
													<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="form-group m-form__group row align-items-center">
																	<div class="col-md-4">
																		<div class="m-input-icon m-input-icon--left">
																			<input type="text" class="form-control m-input" placeholder="Search..." id="cari">
																			<span class="m-input-icon__icon m-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="cobass"></div>												
												</div>
												<!--end:: Widgets/Daily Sales-->
											</div>

<div class="col-xl-6">
	<div class="m-widget14">
		<div class="st-saldo">
			<label><?= date('d-m-Y'); ?></label>
			<label class="jam"></label>
			<span>Saldo</span>
			<label>Rp <?= number_format($data_saldo->saldo) ?></label>
		</div>
	</div>
</div>

											<div class="col-xl-6">

												<!--begin:: Widgets/Revenue Change-->
												<div class="m-widget14">
													<div class="m-widget14__header">
														<h3 class="m-widget14__title">
															Persentase penjualan <?= date('Y') ?>
														</h3>
														<span class="m-widget14__desc">
															Produk yang terjual
														</span>
													</div>
													<div class="row  align-items-center">
														<div class="col-sm-5">
															<div id="m_chart_revenue_change" class="m-widget14__chart1" style="height: 180px">
															</div>
														</div>
														<div class="col-sm-7">
															<div class="m-widget14__legends">
																<?php
																$count = count($arr_push3);
																for ($i=0; $i < $count ; $i++) { ?> 
																	<div class="m-widget14__legend">
																		<span class="m-widget14__legend-bullet" style="background:<?= $arr_push3[$i]; ?>"></span>
																		<span class="m-widget14__legend-text"><?= $arr_push4[$i]." (".$arr_push2[$i].")" ?></span>
																	</div>
																<?php }	?>
															</div>
														</div>
														
													</div>
												</div>

												<!--end:: Widgets/Revenue Change-->
											</div>


											<div class="col-xl-6">

												<!--begin:: Widgets/Daily Sales-->
												<div class="m-widget14">
													<div class="m-widget14__header m--margin-bottom-30">
														<h3 class="m-widget14__title">
															Penjualan Terakhir <?= date('Y') ?> (quantity)
														</h3>
														<span class="m-widget14__desc">
															Detail jumlah penjualan anda dalam perbulan
														</span>
													</div>
													<div class="m-widget14__chart" style="height:120px;">
														<canvas id="m_chart_daily_sales"></canvas>
													</div>												
												</div>
												<!--end:: Widgets/Daily Sales-->
											</div>
											
										</div>
									</div>
								</div>

								<!--End::Section-->
							</div>
	</div>