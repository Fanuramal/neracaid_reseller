<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Product Terbaru | Nineswidosari</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" href="<?= base_url('assets/uploads/avatars/nineswidosari.png') ?>">

    <link rel="stylesheet" href="<?= base_url('styleCapture/css/open-iconic-bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/animate.css') ?>">
    
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/magnific-popup.css') ?>">

    <link rel="stylesheet" href="<?= base_url('styleCapture/css/aos.css') ?>">

    <link rel="stylesheet" href="<?= base_url('styleCapture/css/ionicons.min.css') ?>">

    <link rel="stylesheet" href="<?= base_url('styleCapture/css/bootstrap-datepicker.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/jquery.timepicker.css') ?>">

    
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/flaticon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/icomoon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('styleCapture/css/style.css') ?>">
  </head>
  <body>
    <div id="colorlib-page">
        <!-- <div id="colorlib-main"> -->
            <section class="ftco-section-2">        
              <div class="container">
                <div class="row no-gutters slider-text align-items-center">
                  <div class="col-md-9 ftco-animate">
                    <h1 class="mb-3 bread">Produk Terbaru</h1>
                  </div>
                  <div class="col-md-12 ftco-animate">
                    <center><a href="https://nineswidosari.com/readystock" class="btn btn-info" target="_blank">Lihat Gallery Ready Stock</a></center>
                  </div>
                </div>
              </div>
                <div class="photograhy">
                    <div class="row no-gutters">
                        <?php foreach ($image as $key => $value) { ?>
                        <div class="col-md-4 ftco-animate">
                            <a href="<?= base_url('assets/uploads/').$value->image ?>" class="photography-entry img image-popup d-flex justify-content-center align-items-center" style="background-image: url(<?= base_url('assets/uploads/').$value->image ?>)">
                                <div class="overlay"></div>
                                <div class="text text-center">
                                    <h3><?= $value->name ?></h3>
                                    <span class="tag"><?= $value->code ?></span>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
        <footer class="ftco-footer ftco-bg-dark ftco-section">
          <div class="container px-md-5">
            <div class="row">
              <div class="col-md-12">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This product is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://nineswidosari.com" target="_blank">Nineswidosari</a>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
              </div>
            </div>
          </div>
        </footer>
        <!-- </div> -->
        <!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?= base_url('styleCapture/') ?>js/jquery.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/popper.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.easing.1.3.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.waypoints.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.stellar.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/owl.carousel.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.magnific-popup.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/aos.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.animateNumber.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/bootstrap-datepicker.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/jquery.timepicker.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/scrollax.min.js"></script>
  <script src="<?= base_url('styleCapture/') ?>js/main.js"></script>
    
  </body>
</html>