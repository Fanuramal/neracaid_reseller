<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Online Reseller Monitoring System</title>

		<link rel="shortcut icon" href="<?= base_url('assets/uploads/avatars/nineswidosari.png') ?>">
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <style>
        	.hilang{display: none;}
        	@media (max-width: 720px){
        		.r-hidden{display: none !important;}
        	}
        </style>
		<!--end::Web font -->
		<!--begin:: Global Mandatory Vendors -->
		<link href="<?= base_url() ?>styleMetronic/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?= base_url() ?>styleMetronic/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="<?= base_url() ?>styleMetronic/assets/demo/base/style.bundle.min.css" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/uploads/companies/logo-fix.png') ?>">

		<!-- Media Print web -->
		<link rel="stylesheet" href="<?= base_url() ?>styleMetronic/vendors/media_print.css" />
			
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

		<!-- begin::Page loader -->
		<div class="m-page-loader m-page-loader--base">
			<div class="m-blockui">
				<span>Please wait...</span>
				<span>
					<div class="m-loader m-loader--brand"></div>
				</span>
			</div>
		</div>

		<!-- end::Page Loader -->
		
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">