<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ubah Password | Online Reseller</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		#logos-change-pass{width: 120px;height: auto;}
	</style>
<!--===============================================================================================-->
		<link rel="shortcut icon" href="<?= base_url('assets/uploads/avatars/logo-fix.png') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/bootstrap/css/bootstrap.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/animate/animate.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/css-hamburgers/hamburgers.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/animsition/css/animsition.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/select2/select2.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/vendor/daterangepicker/daterangepicker.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/css/util.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('styleMetronic/change_pass/css/main.css') ?>">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<!-- <form class="contact100-form validate-form"> -->
<?php  
$arr = array('class' => 'contact100-form validate-form');
echo form_open('onlinereseller/update_pass',$arr);
?>
				<span class="contact100-form-title">
					<!-- Buat Password -->
					<img src="<?= base_url('styleMetronic/assets/app/media/img/logos/Nineswidosari_co.png') ?>" id="logos-change-pass">
				</span>
				<!-- <label class="label-input100" for="user_one">Username *</label>
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="user_one" id="user_one" data-validate="Input your Username" placeholder="Username">
					<span class="focus-input100"></span>
				</div> -->
				<label class="label-input100" for="user_one" style="background-color: #584ea1; color: white">Email *</label>
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" value="<?= $get_email ?>" disabled>
					<input class="input100" type="hidden" name="email_one" id="email_one" value="<?= $get_email ?>">
					<!-- <input class="input100" type="text" name="user_one" id="user_one" data-validate="Input your Username" placeholder="Username"> -->
					<span class="focus-input100"></span>
				</div>

				<label class="label-input100" for="first-name" style="background-color: #584ea1; color: white">Password *</label>
				<div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Type Password">
					<input id="password_one" class="input100" type="password" name="password_one" placeholder="Password">
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 rs2-wrap-input100 validate-input" data-validate="Type Verification">
					<input class="input100" type="password" name="password_two" id="password_two" placeholder="Verification password">
					<span class="focus-input100"></span>
				</div>
					<span id="info_pass"></span>
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn" type="submit">
						Simpan
					</button>
				</div>
			<!-- </form> -->
			<?= form_close(); ?>

			<div class="contact100-more flex-col-c-m" style="background-image: url(<?= base_url('styleMetronic/change_pass/images/bg-01.jpg') ?>)" >
				<div class="flex-w size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-map-marker"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							Alamat
						</span>

						<span class="txt2">
							Graha Bukit Raya 3 C4 No 21-22, Cilame<br>Bandung Barat, Jawa Barat 40552<br>Indonesia
						</span>
					</div>
				</div>

				<div class="dis-flex size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-phone-handset"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							Kontak
						</span>

						<span class="txt3">
							62+ 812-2054-5969
						</span>
					</div>
				</div>

				<div class="dis-flex size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-envelope"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							General Support
						</span>

						<span class="txt3">
							konsultanreseller@gmail.com
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/animsition/js/animsition.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/bootstrap/js/popper.js') ?>"></script>
	<script src="<?= base_url('styleMetronic/change_pass/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/select2/select2.min.js') ?>"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/daterangepicker/moment.min.js') ?>"></script>
	<script src="<?= base_url('styleMetronic/change_pass/vendor/daterangepicker/daterangepicker.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/vendor/countdowntime/countdowntime.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('styleMetronic/change_pass/js/main.js') ?>"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
	<script>
		$(document).ready(function(){
			$('#password_two').focusout(function(){
			    checkpass();
			});
			// $('#email_one').focusout(function(){
			//     checkemail();
			// });
			function checkpass(){
			    a = $('#password_one').val();
			    b = $('#password_two').val();
			    var html = '';
			        if (a != b) {
			            html += "<b style='color:red'> Password anda tidak sama, silakan cek kembali</b>";
			                }
			            $('#info_pass').html(html);   
			    }
			// function checkemail(){
			//     a = $('#email_one').val();
			//     b = $('#email_two').val();
			//     var html = '';
			//         if (a != b) {
			//             html += "<b style='color:red'>Email invalid</b>";
			//                 }
			//             $('#info_email').html(html);   
			//     }
		});
	</script>
</body>
</html>
