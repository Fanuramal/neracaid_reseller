<?php $this->load->view('default/agen/_base/header'); ?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?= base_url('styleMetronic/assets/app/media/img/logos/Nineswidosari_co.png') ?>" style="height: 150px">
									</a>
								</div>
								<div class="m-login__signin">
<!-- 									<div class="m-login__head">
										<h3 class="m-login__title">Sign In</h3>
									</div> -->
<?php
$attributes = array('class' => 'm-login__form m-form','novalidate' => 'novalidate');
echo form_open('onlinereseller/checkHelp',$attributes);
echo $this->session->flashdata('wrong');
?>
										<div class="row m-login__form-sub">
											<div class="col m--align-left">
												<label class="label">
													Anda sudah Daftar di <a href="https://bit.ly/Daftar-resellerNW">https://bit.ly/Daftar-resellerNW</a> Lalu ada masalah :<br>
													1) Tidak bisa membuka Link "Create Password" di Email. <br>
													2) Pesan Email [konfirmasi Create Password] tidak masuk ke E-Mail anda.<br><br>
													<center>
													<b>Dengan Catatan</b><br><b> "Admin sudah menghubungi anda melalui WhatsApp atau lainnya..."</b><br><br></center> Silakan Isi Email anda sesuai yang anda daftarkan sebelumnya
												</label>
											</div>
										</div>
										<div class="form-group m-form__group has-danger">
											<input class="form-control m-input" type="email" placeholder="Email" name="identity" autocomplete="off" aria-describedby="email-error" aria-invalid="true" required>
										</div>
										<div class="m-login__form-action">
											<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Send</button>
											<input type="submit" value="Login" class="hilang" id="check-login">
										</div>
										<input type="hidden" name="getCodeStock" value="<?= $this->input->get('getStock') ?>">
										<?= form_close(); ?>
									<!-- </form> -->
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--center">
							<!-- <div class="m-login__account">
								<span class="m-login__account-msg">
									Don't have an account yet ?
								</span>&nbsp;&nbsp;
								<a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">Sign Up</a>
							</div> -->
						</div>
					</div>
				</div>
				<!-- <div class="r-hidden"> -->
					<div class="r-hidden m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style="background-image: url(<?= base_url('styleMetronic/assets/app/media/img//bg/bg-4.jpg') ?>)">
						<div class="m-grid__item">
							<h3 class="m-login__welcome" align="center">Bila Ada Kesulitan Harap Hubungi Kami</h3>
						</div>
					</div>
				<!-- </div> -->
			</div>
<?php $this->load->view('default/agen/_base/footer'); ?>