
			<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--responsive m-container--xxl">
					<div class="m-footer__wrapper">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									<!-- 2018 &copy; Neraca.ID by <a href="https://keenthemes.com" class="m-link">Keenthemes</a> -->
									2019 &copy; Neraca.ID
								</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- end:: Page -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<!--end::Page Vendors -->

		<script>
			var DatatableDataLocalDemos = function () {
	//== Private functions

	// demo initializer
	var demos = function () {
		var dataJSONArray = JSON.parse('<?= $data_fee ?>');
		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'local',
				source: dataJSONArray,
				pageSize: 10
			},

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
				// height: 450, // datatable's body's fixed height
				footer: true // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// inline and bactch editing(cooming soon)
			// editable: false,

			// columns definition
			columns: [{
				field: "id",
				title: "ID",
          		textAlign: 'center'
			}, {
				field: "tgl",
				title: "Tanggal"
			}, {
				field: "customer",
				title: "Customer",
          		textAlign: 'center'
			}, {
				field: "total",
				title: "Total",
          		textAlign: 'right'
			}, {
				field: "debit",
				title: "Debit",
          		textAlign: 'right'
			}, {
				field: "kredit",
				title: "Kredit",
          		textAlign: 'right'
			}, {
				field: "keterangan",
				title: "Keterangan",
          		textAlign: 'center'
			}]
		});

	};

	return {
		//== Public functions
		inits: function () {
			// init dmeo
			demos();
		}
	};
}();

$(document).ready(function () {
	DatatableDataLocalDemos.inits();
});
		</script>
		<!--begin::Page Scripts -->
		<?php $this->load->view('default/agen/dashboard/dashboard'); ?>
		<!--end::Page Scripts -->

<script>
	$(document).ready(function(){

/* Fungsi formatRupiah */
function formatRupiah(bilangan){
	var	number_string = bilangan.toString(),
		sisa 	= number_string.length % 3,
		rupiah 	= number_string.substr(0, sisa),
		ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
			
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	return "Rp "+rupiah;
}

		$("#btn_check_1").on("click",function(){
			var pcid = $('#check_id_stock').val();
		$.ajax({
            type : "GET",
            url  : "<?= base_url('onlinereseller/welcome/check_stock3') ?>",
            dataType : "json",
            data : {pcid:pcid},
            success: function (data) {
            	console.log(data.length);
var html1 = "";
var tr = "";
var i = 0;
for(var g=0; g<data.length; g++){

	// var x = data[g].s_table;
	// for (var j=0; j<x.length; j++) {
		// console.log(x[j]);	
		// tr += "<tr><td>"+x[j].replace(/,/g,"</td><td>")+"</td></tr>";
		tr += "<tr><td>"+data[g].variant+"</td><td>"+data[g].qty+"</td><td>"+formatRupiah(data[g].price)+"</td></tr>";
	// }
}
html1 += '<div class="col-xl-6" style="margin:auto">'+
			'<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">'+
				'<div class="m-portlet__head m-portlet__head--fit">'+
				'</div>'+
				'<div class="m-portlet__body">'+
					'<div class="m-widget19">'+
						'<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
							'<img src="<?= base_url('assets/uploads/') ?>'+data[0].image+'" alt="" style="max-height:486px;object-fit: contain;">'+
								// '<h3 class="m-widget19__title m--font-light">'+data[i].cname+'</h3>'+
								'<div class="m-widget19__shadow"></div>'+
						'</div>'+
						'<div class="m-widget19__content">'+
							'<div class="m-widget19__header">'+
								'<div class="m-widget19__info">'+
									'<span class="m-widget19__username">'+data[0].name+'</span><br>'+
									'<span class="m-widget19__time">'+data[0].code+'</span>'+
								'</div>'+
								// '<div class="m-widget19__stats">'+
								// 	'<span class="m-widget19__number m--font-brand">'+data[i].total_qty+'</span>'+
								// 	'<span class="m-widget19__comment">Total(Pcs)</span>'+
								// '</div>'+
							'</div>'+
							'<div class="m-widget19__body">'+data[0].description+'</div><p></p>'+
								'<div class="table-responsive">'+
								'<table class="table m-table m-table--head-bg-success table-bordered">'+
									'<thead>'+
										'<tr><th>VARIAN</th><th>QTY</th><th>Harga</th></tr>'+
									'</thead>'+
									'<tbody>'+tr+'</tbody>'+
								'</table>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	                $("#check_1").html(html1);
	                $("#check_2").html("");
            }
           });
		});

		$('.closeauto').on('click',function(){
			$('#m_aside_header_menu_mobile_close_btn').trigger('click');
		});
	});
</script>
<script>
    //== Class definition

var DatatableJsonRemoteDemox = function () {
    //== Private functions

    // basic demo
    var demox = function () {
    	var cobax = JSON.parse('<?= $datatable_m ?>');
        // console.log(cobax);
        var datatablex = $('.cobass').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: cobax,
                pageSize: 10,
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#cari')
            },

            // columns definition
            columns: [{
                field: "name",
                title: "Nama",
                // width: 50,
                // sortable: false,
                // selector: false,
                // textAlign: 'center'
            }, {
                field: "total_harga",
                title: "Total harga",
				type: "number"
            }, {
                field: "total_item",
                title: "Total item",
				type: "number"
            }, {
                field: "tanggal",
                title: "Tanggal",
                type: "date",
                format: "MM/DD/YYYY"
            }]
        });
    };

    return {
        // public functions
        initq: function () {
            demox();
        }
    };
}();

jQuery(document).ready(function () {
    DatatableJsonRemoteDemox.initq();
});
</script>
	</body>

	<!-- end::Body -->
</html>