<?php 
$this->load->view('default/agen/dashboard/head');

$getDataStock = $this->db->query("SELECT 
    C.`id`,
    P.`subcategory_id`,
    C.`code`,
    C.`name`,
    C.`description`,
    IF(C.`image` IS NULL,'no_image.png',C.`image`) AS image,
    V.`name` AS variant, 
    SUM(ROUND(V.`quantity`)) AS qty,
    (ROUND(P.`price`)+ROUND(V.`price`)) AS price
FROM (`sma_categories` C LEFT JOIN `sma_products` P ON P.`subcategory_id` = C.`id`) LEFT JOIN `sma_product_variants` V ON V.`product_id` = P.`id`
WHERE P.`subcategory_id` IS NOT NULL 
AND (V.`quantity` > 0 OR V.`quantity` IS NOT NULL)
AND C.`code`= '".$this->input->get('codestock')."'
GROUP BY
    C.`id`,
    P.`subcategory_id`,
    C.`name`,
    C.`image`,
    V.`name`
ORDER BY C.`id` DESC");

if ($this->input->get('codestock')) {
	$data = array('stockes' => 'active', 'homes' => '', 'getDataStock' => $getDataStock);
}
else{
	$data = array('stockes' => '', 'homes' => 'active');
}
?>

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid m-grid--hor m-container m-container--responsive m-container--xxl">
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
					<div class="m-grid__item m-body__nav">
						<div class="m-stack m-stack--ver m-stack--desktop">

							<!-- begin::Horizontal Menu -->
							<div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
								<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
									<ul class="nav m-menu__nav  m-menu__nav--submenu-arrow" role="tablist">
										<li class="m-menu__item" aria-haspopup="true">
											<a href="#" class="m-menu__link closeauto" data-toggle="tab" data-target="#tabs_1">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">Dashboard</span>
											</a>
										</li>
										<li class="m-menu__item m-menu__item--submenu m-menu__item--tabs" aria-haspopup="true">
											<a class="m-menu__link closeauto" data-toggle="tab" href="#tabs_2" role="tab" aria-selected="false">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">Komisi & Pembayaran</span>
											</a>
										</li>
										<li class="m-menu__item m-menu__item--submenu m-menu__item--tabs" aria-haspopup="true">
											<a class="m-menu__link closeauto" href="<?= base_url('agen/welcome/gallery/')?>" aria-selected="false" target="_self">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">Produk Terbaru</span>
											</a>
										</li>
										<li class="m-menu__item m-menu__item--submenu m-menu__item--tabs" aria-haspopup="true">
											<a class="m-menu__link closeauto" data-toggle="tab" href="#tabs_4" role="tab" aria-selected="true">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">Cek Stok</span>
											</a>
										</li>
										<li class="m-menu__item m-menu__item--submenu m-menu__item--tabs" aria-haspopup="true">
											<a class="m-menu__link closeauto" href="https://nineswidosari.com/panduan-berjualan-online-nw/" aria-selected="false" target="_blank">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">Panduan Berjualan</span>
											</a>
										</li>
										<!-- <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link"><span class="m-menu__item-here"></span><span
												 class="m-menu__link-text">Orders</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a>
											<div class="m-menu__submenu  m-menu__submenu--fixed-xl m-menu__submenu--center"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
												<div class="m-menu__subnav">
													<ul class="m-menu__content">
														<li class="m-menu__item">
															<h3 class="m-menu__heading m-menu__toggle"><span class="m-menu__link-text">HR Reports</span><i class="m-menu__ver-arrow la la-angle-right"></i></h3>
															<ul class="m-menu__inner">
																<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="inner.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Staff
																			Directory</span></a></li>
																
															</ul>
														</li>
													</ul>
												</div>
											</div>
										</li> -->
									</ul>
								</div>
							</div>

							<!-- end::Horizontal Menu -->
						</div>
					</div>
					<div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-body__content">
						<div class="m-grid__item m-grid__item--fluid m-wrapper">
<div class="tab-content">
	<?php 
		$this->load->view('default/agen/_menu/home',$data);
		$this->load->view('default/agen/_menu/fee_payment');
		$this->load->view('default/agen/_menu/stock',$data);
	?>
</div>
						</div>
					</div>
				</div>
			</div>

			<!-- begin::Body -->
<?php $this->load->view('default/agen/dashboard/foot'); ?>