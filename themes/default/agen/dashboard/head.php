<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Online Reseller Monitoring System</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="<?= base_url('assets/uploads/avatars/nineswidosari.png') ?>">

<style>
div.st-saldo{
	width: 80%;
	padding: 10px;
	border-radius: 10px;
	border: solid gray 5px;
	margin: auto;
	text-align: center;
}
div.st-saldo label{
	display: block;
	font-weight: bold;
	font-size: 33px;
}
div.st-saldo span{
	font-weight: bold;
	font-size: 17px;
	text-decoration: underline;
}
.alligator-turtle {
  object-fit: cover;

  width: 300px;
  height: 337px;
}
div.gallery_s {border: 1px solid #ccc;}
div.gallery_s:hover {border: 1px solid #777;}
div.gallery_s img {
  object-fit: cover;

  width: 100%;
  height: 200px;}
div.desc_s {padding: 15px;text-align: center;font-size: 17px;font-weight: bold;}
.border_box > * {box-sizing: border-box;}
.responsive_s {padding: 0 6px;float: left;width: 24.99999%;}
.show-image{width: 20%;height: auto;margin: auto;}
@media only screen and (max-width: 700px) {
  .responsive_s {width: 49.99999%;margin: 6px 0;}
.show-image{width: 50%}
div.st-saldo label{font-size: 23px}
}
@media only screen and (max-width: 500px) {
  .responsive_s {width: 100%;}
div.st-saldo label{font-size: 23px}
}
.clearfix_s:after {content: "";display: table;clear: both;}
</style>
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?= base_url() ?>styleMetronic/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?= base_url() ?>styleMetronic/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

		<link href="<?= base_url() ?>styleMetronic/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>styleMetronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		    function jam() {
		    var time = new Date(),
		        hours = time.getHours(),
		        minutes = time.getMinutes(),
		        seconds = time.getSeconds();
		    document.querySelectorAll('.jam')[0].innerHTML = harold(hours) + ":" + harold(minutes) + ":" + harold(seconds);
		      
		    function harold(standIn) {
		        if (standIn < 10) {
		          standIn = '0' + standIn
		        }
		        return standIn;
		        }
		    }
		    setInterval(jam, 1000);
		</script>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body style="background-image: url(<?= base_url() ?>styleMetronic/assets/app/media/img/bg/bg-1.jpg)" class="m-page--boxed m-body--fixed m-header--static m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- begin::Header -->
			<header id="m_header" class="m-grid__item	m-grid m-grid--desktop m-grid--hor-desktop  m-header ">
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--hor-desktop m-container m-container--responsive m-container--xxl">
					<div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-header__wrapper">

						<!-- begin::Brand -->
						<div class="m-grid__item m-brand">
							<div class="m-stack m-stack--ver m-stack--general m-stack--inline">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="#" class="m-brand__logo-wrapper">
										<!-- <img alt="" src="<?= base_url() ?>styleMetronic/assets/demo/media/img/logo/logo.png" /> -->
										<img alt="" src="<?= base_url('assets/uploads/logos/').$this->session->userdata('logo') ?>" style="height: 35px" />

									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- begin::Responsive Header Menu Toggler-->
									<!-- <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block"> -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class=" m--visible-tablet-and-mobile-inline-block">
										<label style="color: white; font-size: 16px; font-weight: bold;">MENU</label>
									</a>

									<!-- end::Responsive Header Menu Toggler-->

									<!-- begin::Topbar Toggler-->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

									<!--end::Topbar Toggler-->
								</div>
							</div>
						</div>

						<!-- end::Brand -->

						<!-- begin::Topbar -->
						<div class="m-grid__item m-grid__item--fluid m-header-head" id="m_header_nav">
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
										 m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__welcome m--hidden-tablet m--hidden-mobile">Hello,&nbsp;</span>
												<span class="m-topbar__username m--hidden-tablet m--hidden-mobile m--padding-right-15"><span class="m-link"><?= $this->session->userdata("name") ?></span></span>
												<span class="m-topbar__userpic">
													<!-- <img src="<?= base_url() ?>styleMetronic/assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt="" /> -->
													<span class="fa fa-power-off"></span>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url(<?= base_url() ?>styleMetronic/assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500">
																	<?= $this->session->userdata('name') ?>
																</span>
																<a href="#" class="m-card-user__email m--font-weight-300 m-link">
																	<?= $this->session->userdata('email_agen') ?></a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">Section</span>
																</li>
																<li class="m-nav__separator m-nav__separator--fit">
																</li>
																<li class="m-nav__item">
																	<a href="<?= base_url('onlinereseller/welcome/logout_agen'); ?>" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<!-- end::Topbar -->
					</div>
				</div>
			</header>

			<!-- end::Header -->