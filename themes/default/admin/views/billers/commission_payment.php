<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$v = "";
if ($this->input->post('startdate') != '') {
    $v .= "&sd=" . $this->input->post('startdate');
}
if ($this->input->post('enddate') != '') {
    $v .= "&ed=" . $this->input->post('enddate');
}
?>
<script>
    $(document).ready(function () {
        var cTable = $('#CusData').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "bSort": false,
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': false,
            'sAjaxSource': '<?= admin_url('reseller/getBalance?v=1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, null, null, null, null, null, null, null],
            "aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {
                    // return data +' '+ row[6];
  var x = '<p> Apa kamu yakin? </p> <a class="btn btn-danger" href="<?= admin_url('reseller/deleteCredit?id=') ?>'+data+'"> Ya </a> <button class="btn po-close"> Tidak </button>';
  var y = "<a href='#' class='tip po' title='Hapus Kredit' data-content='"+x+"' data-html='true' rel='popover'> <i class='fa fa-trash-o'></i></a>";
                    return "<div class='text-center'>"
                    +"<a class='tip' href='<?= admin_url('reseller/editCredit?id=') ?>"+data+"' data-toggle='modal' data-target='#myModal'><i class='fa fa-edit'></i></a>"
                    +y
                    +"</div>"
                    +"</div>";
                },
                "aTargets": [ 7 ],
            }
        ],
        "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
            
                        var gtotalD = 0, gtotalC = 0;
                        for (var i = 0; i < aaData.length; i++) {
                            gtotalC += parseFloat(aaData[aiDisplay[i]][5]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[1].innerHTML = currencyFormat(parseFloat(gtotalC));
        }
        });

        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $("#startdate, #enddate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                format: 'dd-mm-yyyy'
            }).datetimepicker('update');
    });

</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('commission_payment'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <?= $this->session->flashdata('message'); ?>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                
                <div id="form">

                    <?php echo admin_form_open("reseller/listBalance"); ?>
                    <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tanggal Awal</label>
                                    <input type="text" name="startdate" class="form-control" id="startdate" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tanggal Akhir</label>
                                    <input type="text" name="enddate" class="form-control" id="enddate" autocomplete="off">
                                </div>
                            </div>
                    </div>
                    <div class="form-group">
                        <div
                            class="controls"> <?php echo form_submit('submit_reports', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
                <div class="clearfix"></div>

                <div class="table-responsive">
                    <table id="CusData" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th><?= lang('date') ?></th>
                            <th><?= lang('name') ?></th>
                            <th><?= lang('bcf2') ?></th>
                            <th><?= lang('bcf1') ?></th>
                            <th><?= lang('bcf4') ?></th>
                            <th><?= lang('kredit') ?></th>
                            <th><?= lang('description') ?></th>
                            <th><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th colspan="5">Total</th>
                            <th></th>
                            <th colspan="2"></th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reseller/getBalance/0/xls?v=1'.$v) ?>";
            return false;
        });
    });
</script>