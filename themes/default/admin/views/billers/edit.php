<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_biller'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("reseller/edit/" . $biller->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("logo", "biller_logo"); ?>
                        <?php
                        $biller_logos[''] = '';
                        foreach ($logos as $key => $value) {
                            $biller_logos[$value] = $value;
                        }
                        echo form_dropdown('logo', $biller_logos, $biller->logo, 'class="form-control select" id="biller_logo" required="required" '); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="logo-con" class="text-center"><img
                            src="<?= base_url('assets/uploads/logos/' . $biller->logo) ?>" alt=""></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group company">
                        <?= lang("company", "company"); ?>
                        <?php echo form_input('company', $biller->company, 'class="form-control tip" id="company" required="required"'); ?>
                    </div>
                    <div class="form-group person">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', $biller->name, 'class="form-control tip" id="name" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', $biller->vat_no, 'class="form-control" id="vat_no"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("gst_no", "gst_no"); ?>
                        <?php echo form_input('gst_no', $biller->gst_no, 'class="form-control" id="gst_no"'); ?>
                    </div>
                    <!--<div class="form-group company">
                    <?= lang("contact_person", "contact_person"); ?>
                    <?php //echo form_input('contact_person', $biller->contact_person, 'class="form-control" id="contact_person" required="required"'); ?>
                </div> -->
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" class="form-control" required="required" id="email_address"
                               value="<?= $biller->email ?>"/>
                    </div>
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel" name="phone" class="form-control" required="required" id="phone"
                               value="<?= $biller->phone ?>"/>
                    </div>
                    <div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', $biller->address, 'class="form-control" id="address" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php echo form_input('city', $biller->city, 'class="form-control" id="city" required="required"'); ?>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("state", "state"); ?>
                        <?php
                        if ($Settings->indian_gst) {
                            $states = $this->gst->getIndianStates();
                            echo form_dropdown('state', $states, $biller->state, 'class="form-control select" id="state" required="required"');
                        } else {
                            echo form_input('state', $biller->state, 'class="form-control" id="state"');
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php echo form_input('postal_code', $biller->postal_code, 'class="form-control" id="postal_code"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("country", "country"); ?>
                        <?php echo form_input('country', $biller->country, 'class="form-control" id="country"'); ?>
                    </div>

<!-- Chose Bank -->
                    <div class="form-group">
                        <?= lang("bcf1", "cf1"); ?>
                        <?php
                        $xbank1 = "";
                        $xbank2 = "";
                        $push_bank[''] = '';
                        $arr_bank = array('mandiri' => "MANDIRI",'bca' => "BCA",'bni' => "BNI", 'bri' => "BRI", 'dan lain-lain' => "Dan lain-lain");
                        foreach ($arr_bank as $key => $value) {
                            $push_bank[$value] = $value;
                        }
    $data_banks = array(
        'name' => "cf1",
        'id' => "bank_second",
        'class' => "form-control",
        'placeholder' => "Bank lainnya...",
        'value' => $biller->cf1
    );
    // echo form_dropdown('cf1', $push_bank, $biller->cf1, 'class="form-control select" id="cf1"  required="required"');
    echo form_input($data_banks);
    ?>
                    </div>
<!-- =========== -->
                    <div class="form-group">
                        <?= lang("bcf2", "cf2"); ?>
                        <?php echo form_input('cf2', $biller->cf2, 'class="form-control" id="cf2" required="required"'); ?>

                    </div>

                    <div class="form-group">
                        <?= lang("bcf4", "cf4"); ?>
                        <?php echo form_input('cf4', $biller->cf4, 'class="form-control" id="cf4"'); ?>

                    </div>
                <div class="col-md-6">
                    <div class="form-group all">
                        <label for="product_image">KTP</label>
<span class="file-input file-input-new">
<div class="input-group ">
   <input type="file" name="ktp_image">
</div>
</span>
                    </div>
                </div>
                    <div class="hilang">
                    <div class="form-group">
                        <?= lang("bcf3", "cf3"); ?>
                        <?php echo form_input('cf3', $biller->cf3, 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf5", "cf5"); ?>
                        <?php echo form_input('cf5', $biller->cf5, 'class="form-control" id="cf5"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf6", "cf6"); ?>
                        <?php echo form_input('cf6', $biller->cf6, 'class="form-control" id="cf6"'); ?>
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("invoice_footer", "invoice_footer"); ?>
                        <?php echo form_textarea('invoice_footer', $biller->invoice_footer, 'class="form-control skip" id="invoice_footer" style="height:100px;"'); ?>
                    </div>
                </div>
            </div>
Sementara ada perubahan Status saat promo Ramadan <br>
Ingin tukar menjadi Stokis? <button class="btn btn-success" type="button" id="changeYes">Yes</button> | <button class="btn btn-danger" type="button" id="changeNo">No</button><br><span id="pesan"></span>
<input type="hidden" name="gi" id="gi" placeholder="group-id"><br>
<input type="hidden" name="gn" id="gn" placeholder="group-name" value="biller"><br>
<input type="hidden" name="cgi" id="cgi" placeholder="customer-group-id"><br>
<input type="hidden" name="cgn" id="cgn" placeholder="customer-group-name">
</div>
        <div class="modal-footer">
            <?php echo form_submit('edit_biller', lang('edit_biller'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#biller_logo').change(function (event) {
            var biller_logo = $(this).val();
            $('#logo-con').html('<img src="<?=base_url('assets/uploads/logos')?>/' + biller_logo + '" alt="">');
        });
        $('#changeYes').click(function(){
            $('#gi').val('3');
            $('#gn').val('customer');
            $('#cgi').val('2');
            $('#cgn').val('Stokis');
            $('#pesan').html("Sukses terisi");
        });
        $('#changeNo').click(function(){
            $('#gi').val('');
            $('#gn').val('biller');
            $('#cgi').val('');
            $('#cgn').val('');
            $('#pesan').html("Gagal terisi");
        });
    });
</script>
<?= $modal_js ?>