<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

require(APPPATH.'libraries/lib_mailgun/vendor/autoload.php');
use Mailgun\Mailgun;

class Agen extends CI_Controller
{

    function __construct()
    {
    	parent::__construct();
        $this->load->model('agen/m_agen');
        $this->loggedIn = $this->sma->logged_in_agen();
        $getStock = $this->input->get('getStock');
        if ($this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            if ($getStock != '') {
                redirect('onlinereseller/welcome?codestock='.$getStock);
            }
            else{
                redirect('onlinereseller/welcome');
            }
        }
    }

    function index(){
    	$this->load->view('default/agen/login');
    }

    function check_login(){
        $getCodeStock = $this->input->post('getCodeStock');
        $user = $this->input->post('identity');
        $pass = $this->input->post('password');
        $remember = $this->input->post('remember');

        $data = $this->m_agen->check_login($user,$pass);
        if ($data) {
            if (!empty($remember)) {
                $cookie1 = array(
                    'name' => 'dsghajbvbxcayfgevwqbhjdhjksahn',
                    'value'=> $user,
                    'expire'=>'3600');
                set_cookie($cookie1);
                $cookie2 = array(
                    'name' => 'hdjsbvvewyuwqgvjdjxsakldjkbwnq',
                    'value'=> $pass,
                    'expire'=>'3600');
                set_cookie($cookie2);
            }
            else{
                $cookie1 = array(
                    'name' => 'dsghajbvbxcayfgevwqbhjdhjksahn',
                    'value'=> '');
                set_cookie($cookie1);
                $cookie2 = array(
                    'name' => 'hdjsbvvewyuwqgvjdjxsakldjkbwnq',
                    'value'=> '');
                set_cookie($cookie2);
            }
            if (!empty($getCodeStock)) {
                redirect('onlinereseller/welcome?codestock='.$getCodeStock);
            }else{
                redirect('onlinereseller/welcome');
            }
        }
        else{
            $this->session->set_flashdata('wrong', '<div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <span>Email / Password anda salah, tolong periksa kembali</span>
        </div>');
            if (!empty($getCodeStock)) {
                redirect('onlinereseller?getStock='.$getCodeStock);
            }else{
                redirect('onlinereseller');
            }
        }
    }

    function change_pass(){

        $emails = $this->input->get('emails');
        $str_emails = str_replace(" ", "+", $emails);
        $str_emails2 = str_replace("PLUS", "+", $str_emails);
        $data = array(
            'get_email' => $this->encryption->decrypt($str_emails2)
        );
        $query = $this->db->get_where('companies', array('email' => $data['get_email']));
        $row = $query->row();

        if ($row->change_pass == 0 || !empty($row->password)) {
            $this->load->view('default/agen/change_pass',$data);
        }else{
            redirect('onlinereseller');
        }
    }

    function update_pass(){
        $a = $this->input->post('password_one');
        $b = $this->input->post('password_two');
        $c = $this->input->post('email_one');
        if ($a == $b && $c != '') {
            $data = $this->m_agen->update_pass($b,$c); //pass & email
            if ($data) {
                    $mgClient = new Mailgun('4fdce273820843c1356696bd20a27854-c9270c97-5d337f3f');
                    $domain = "neraca.id";

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                        'from'    => 'Neraca.Id <noreply@neraca.id>',
                        'to'      => 'You<'.$c.'>',
                        'subject' => 'Update akun sukses [Reseller Online]',
                        'html'    => 'Selamat terhadap '.$c.'...!<br>Sekarang password anda telah dirubah / sukses dibuat.'
                ));
                redirect('onlinereseller');
            }
        }else{
            $_SERVER['HTTP_REFERER'];
        }
        return false;
    }

    function forgotPassword(){
            $a = $this->input->post('email');
            $mgClient = new Mailgun('4fdce273820843c1356696bd20a27854-c9270c97-5d337f3f');
            $domain = "neraca.id";

                    # Make the call to the client.
            $result = $mgClient->sendMessage($domain, array(
                'from'    => 'Neraca.Id <noreply@neraca.id>',
                'to'      => 'You<'.$a.'>',
                'subject' => 'Forgot Your Password [Reseller Online]',
                'html'    => 'Apa benar ini sebagai '.$a.'...?<br>Bila benar, silakan klik <a href="'.base_url("onlinereseller/fp?lime=".$this->encryption->encrypt($a)).'">Ubah Password</a> untuk mengganti password anda. bila bukan anda, silakan abaikan pesan ini'
                ));
            $this->session->set_flashdata('wrong', '<div class="m-alert m-alert--outline alert alert-brand alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <span>Silakan periksa pesan email anda.</span>
        </div>');
            redirect('onlinereseller');
    }
    function forgot_pass(){
        $a = $this->input->get('lime');
        $replace = str_replace(" ", "+", $a); // kalau ada tanda + atau %20 itu tandanya SPASI di URL
// echo $this->encryption->decrypt($replace);
        $query = $this->db->get_where('companies', array('email' => $this->encryption->decrypt($replace)));
        $num_rows = $query->num_rows();
        $row = $query->row();

        if ($num_rows == 1 && $row->email == $this->encryption->decrypt($replace)) {
            redirect("onlinereseller/change_pass?emails=".$replace);
        }
        else{
            $this->session->set_flashdata('wrong', '<div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <span>Maaf Email anda tidak terdaftar.</span>
        </div>');
            redirect('onlinereseller');
        }
    }


    function helpLink(){
        $this->load->view('default/agen/notOpenLinkPassword');
    }
    function checkHelp(){
        $email = $this->input->post('identity');

        $checkModel = $this->m_agen->checkHelp($email);
        if ($checkModel) {
           $enc = $this->encryption->encrypt($checkModel);
           redirect('onlinereseller/change_pass?emails='.$enc);
        }else{
            $this->session->set_flashdata('wrong', '<div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <span>Email anda belum dikonfirmasi oleh admin, silakan hubungi WA (0812-2054-5969)</span>
        </div>');
            redirect('onlinereseller/help');
        }
    }
}
?>