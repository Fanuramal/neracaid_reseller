<?php
class Welcome extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        
        if (!$this->session->userdata('email_agen')) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('agen');
        }
        if ($this->uri->segment(1)."/".$this->uri->segment(2) == 'onlinereseller/faq') {
        	$this->faq();
        }
	}

    function logout_agen(){
        $this->session->sess_destroy();
        redirect('onlinereseller');
    }
	function index(){
		// $this->load->view('default/agen/_base/header');
		$data = array(
			'data_sales' => $this->get_last_sales(), 
			'data_omzet' => $this->get_last_product(),
			'data_fee'	 => $this->getAllFee(),
			'data_stock' => $this->getStock(),
			'data_saldo' => $this->check_saldo(),
			'datatable_m' => $this->example(),
		);
		$this->load->view('default/agen/dashboard/index',$data);
		// $this->load->view('default/agen/_base/footer');
	}

	function datatables_metronic($ambil_json){
$data = $alldata = json_decode($ambil_json);

$datatable = array_merge(['pagination' => [], 'sort' => [], 'query' => []], $_REQUEST);

// search filter by keywords
$filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch'])
    ? $datatable['query']['generalSearch'] : '';
if ( ! empty($filter)) {
    $data = array_filter($data, function ($a) use ($filter) {
        return (boolean)preg_grep("/$filter/i", (array)$a);
    });
    unset($datatable['query']['generalSearch']);
}

// filter by field query
$query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
if (is_array($query)) {
    $query = array_filter($query);
    foreach ($query as $key => $val) {
        $data = list_filter($data, [$key => $val]);
    }
}

$sort  = ! empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
$field = ! empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'RecordID';

$meta    = [];
$page    = ! empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
$perpage = ! empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

$pages = 1;
$total = count($data); // total items in array

// sort
usort($data, function ($a, $b) use ($sort, $field) {
    if ( ! isset($a->$field) || ! isset($b->$field)) {
        return false;
    }

    if ($sort === 'asc') {
        return $a->$field > $b->$field ? true : false;
    }

    return $a->$field < $b->$field ? true : false;
});

// $perpage 0; get all data
if ($perpage > 0) {
    $pages  = ceil($total / $perpage); // calculate total pages
    $page   = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
    $page   = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
    $offset = ($page - 1) * $perpage;
    if ($offset < 0) {
        $offset = 0;
    }

    $data = array_slice($data, $offset, $perpage, true);
}

$meta = [
    'page'    => $page,
    'pages'   => $pages,
    'perpage' => $perpage,
    'total'   => $total,
];


// if selected all records enabled, provide all the ids
if (isset($datatable['requestIds']) && filter_var($datatable['requestIds'], FILTER_VALIDATE_BOOLEAN)) {
    $meta['rowIds'] = array_map(function ($row) {
        foreach($row as $first) break;
        return $first;
    }, $alldata);
}


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

$result = [
    'meta' => $meta + [
            'sort'  => $sort,
            'field' => $field,
        ],
    'data' => $data,
];
echo json_encode($result, JSON_PRETTY_PRINT);
	}

	function get_last_sales(){
		$query = $this->db->query("SELECT {$this->db->dbprefix('companies')}.`name`,
		SUM(IF( MONTH(`date`) = 01, `total_items`, 0)) AS bulan_1,
        SUM(IF( MONTH(`date`) = 02, `total_items`, 0)) AS bulan_2 ,
        SUM(IF( MONTH(`date`) = 03, `total_items`, 0)) AS bulan_3 ,
        SUM(IF( MONTH(`date`) = 04, `total_items`, 0)) AS bulan_4 ,
        SUM(IF( MONTH(`date`) = 05, `total_items`, 0)) AS bulan_5 ,
        SUM(IF( MONTH(`date`) = 06, `total_items`, 0)) AS bulan_6 ,
        SUM(IF( MONTH(`date`) = 07, `total_items`, 0)) AS bulan_7 ,
        SUM(IF( MONTH(`date`) = 08, `total_items`, 0)) AS bulan_8 ,
        SUM(IF( MONTH(`date`) = 09, `total_items`, 0)) AS bulan_9 ,
        SUM(IF( MONTH(`date`) = 10, `total_items`, 0)) AS bulan_10 ,
        SUM(IF( MONTH(`date`) = 11, `total_items`, 0)) AS bulan_11 ,
        SUM(IF( MONTH(`date`) = 12, `total_items`, 0)) AS bulan_12 
			FROM {$this->db->dbprefix('sales')}
			LEFT JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('sales')}.`biller_id` = {$this->db->dbprefix('companies')}.`id`
			WHERE `payment_status` = 'paid' AND `sale_status` = 'completed' AND YEAR(NOW()) AND `biller_id` = '".$this->session->userdata("companies_id")."' ");

			foreach($query->result() as $r) {

			               $data = array(
			                    $r->bulan_1,
			                    $r->bulan_2,
			                    $r->bulan_3,
			                    $r->bulan_4,
			                    $r->bulan_5,
			                    $r->bulan_6,
			                    $r->bulan_7,
			                    $r->bulan_8,
			                    $r->bulan_9,
			                    $r->bulan_10,
			                    $r->bulan_11,
			                    $r->bulan_12
			               );
			          }

			          // $output2['datasets'] = array(
			          $output2 = array(
			          	array(
			          		"backgroundColor" => "rgb(0, 197, 220)",
			                 "data" => $data,
			             )
			          );
          $json = json_encode($output2,JSON_NUMERIC_CHECK);
          return $json;
          // echo $json;
	}
	function get_last_product(){
		$query = $this->db->query("SELECT 
			{$this->db->dbprefix('sale_items')}.`product_name` AS name, 
    		SUM({$this->db->dbprefix('sale_items')}.`quantity`) AS quantity
			FROM {$this->db->dbprefix('sale_items')}
			LEFT JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('sale_items')}.`sale_id` = {$this->db->dbprefix('sales')}.`id`
			WHERE {$this->db->dbprefix('sales')}.`biller_id` = '".$this->session->userdata("companies_id")."' AND {$this->db->dbprefix('sales')}.`payment_status` = 'paid'
			GROUP BY {$this->db->dbprefix('sale_items')}.`product_name`");
		$i 			= 0;
		$row 		= $query->row();
		$num_rows 	= $query->num_rows();
		$warna 		= array('#ed3b3c','#ed3bbd','#b03bed','#503bed','#3b87ed','#3bc5ed','#3bede3','#b5ed3b','#ed8f3b','#3bed8f','#ed553b','#edc93b');
		$arr_push1 	= array();
		$arr_push2 	= array();

		foreach ($warna as $key => $value) {
			$i++;
			if($i==($num_rows+1)) {
			    break;
			}else{
			array_push($arr_push1, $value);
			}
		}
		foreach($query->result() as $r) {
				$a = array("label" => $r->name,"value" => $r->quantity);
				array_push($arr_push2, $a);
		}

			          $output2 = 
			          	array(
			          		"element"	=> "m_chart_revenue_change",
			                 "data"		=> $arr_push2,
			                 "colors"	=> $arr_push1
			             );
          $json = json_encode($output2,JSON_NUMERIC_CHECK);
          // $json = json_encode($query);
          return $json;
          // echo $json;
	}
    function getAllFee(){
	    	$query = $this->db->query("SELECT
			{$this->db->dbprefix('fee')}.`id`,
			DATE_FORMAT(`tgl`,'%d-%m-%Y') as `tgl`,
			{$this->db->dbprefix('sales')}.customer as `customer`,
			IF({$this->db->dbprefix('fee')}.`total`IS NULL,NULL,FORMAT({$this->db->dbprefix('fee')}.`total`,0)) as `total`,
			IF(`debit`IS NULL,NULL,FORMAT(`debit`,0)) as `debit`,
			IF(`kredit`IS NULL,NULL,FORMAT(`kredit`,0)) as `kredit`,
			`keterangan`
			FROM {$this->db->dbprefix('fee')} 
			LEFT JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('fee')}.`id_biller` = {$this->db->dbprefix('companies')}.`id`
			LEFT JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('fee')}.`id_sale` = {$this->db->dbprefix('sales')}.`id`
			WHERE {$this->db->dbprefix('companies')}.`id` = '".$this->session->userdata('companies_id')."' ");
	    	
	    	return json_encode($query->result());
    }
    function getStock(){
    	$query = $this->db->query("SELECT `id`,`code`,`name`,`image`,ROUND(`quantity`,0) AS `quantity` FROM {$this->db->dbprefix('products')}");
    	return $query;
    }

    function gallery(){
        $data = array(
            'image' => $this->group_image()
        );
    	$this->load->view('default/agen/_menu/index',$data);
    }

    function getAllGallery($id){
    	$query = $this->db->query("SELECT {$this->db->dbprefix('products')}.`id`,`code`,`name`,`product_id`,`photo` 
    		FROM {$this->db->dbprefix('product_photos')}
    		LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('product_photos')}.`product_id` = {$this->db->dbprefix('products')}.`id`
    		WHERE {$this->db->dbprefix('product_photos')}.`product_id`= '".$id."' ");
    	return $query;
    }
    function check_saldo(){
    	$query = $this->db->query("SELECT ROUND(SUM(IF(`debit` IS NULL,0,`debit`))-SUM(IF(`kredit` IS NULL,0,`kredit`))) AS saldo 
									FROM {$this->db->dbprefix('fee')}
									LEFT JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('fee')}.`id_biller` = {$this->db->dbprefix('companies')}.`id`
									WHERE `id_biller` = '".$this->session->userdata('companies_id')."'");
    	$row = $query->row();
    	return $row;
    }

    function check_stock(){
    	$search = $this->input->get('search');
    	$hsl = $this->db->query("SELECT 
	{$this->db->dbprefix('products')}.`name` AS name,
    {$this->db->dbprefix('products')}.`image` AS image,
    {$this->db->dbprefix('products')}.`product_details` AS product_details,
    {$this->db->dbprefix('categories')}.`name` AS cname,
    group_concat(concat({$this->db->dbprefix('product_variants')}.`name`,' Pcs:',ROUND({$this->db->dbprefix('product_variants')}.`quantity`,0)) separator '</td></tr><tr><td>') AS vname,
    SUM(ROUND({$this->db->dbprefix('product_variants')}.`quantity`,0)) AS total
FROM {$this->db->dbprefix('products')}
LEFT JOIN {$this->db->dbprefix('categories')} ON {$this->db->dbprefix('categories')}.`id` = {$this->db->dbprefix('products')}.`category_id`
LEFT JOIN {$this->db->dbprefix('product_variants')} ON {$this->db->dbprefix('product_variants')}.`product_id` = {$this->db->dbprefix('products')}.id
WHERE {$this->db->dbprefix('categories')}.`code` LIKE '%".$search."%'
GROUP BY {$this->db->dbprefix('products')}.`id`");
        if($hsl->num_rows()>0){
// OR {$this->db->dbprefix('products')}.`name`LIKE '%".$search."%'
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'name' 		=> $data->name,
                    'image' 	=> $data->image,
                    'cname'		=> $data->cname,
                    'vname' 	=> $data->vname,
                    'total' 	=> $data->total,
                    'product_details' => $data->product_details,
                    'length'	=> $hsl->num_rows(),
                    );
            }
        	// echo json_encode($hasil,JSON_NUMERIC_CHECK);
        	echo json_encode($hsl->result(),JSON_NUMERIC_CHECK);
        }else{
                $hasil=array(
                    'name' 		=> 0,
                    'image' 	=> 0,
                    'cname' 	=> 0,
                    'vname' 	=> 0,
                    'total' 	=> 0,
                    'product_details' => 0,
                    'length'	=> $hsl->num_rows(),
                    );
        	// echo json_encode($hasil,JSON_NUMERIC_CHECK);
        	echo json_encode($hsl->result(),JSON_NUMERIC_CHECK);
        }
    }

    function check_stock3(){
        $arr = array();
    	$pcid = $this->input->get('pcid');
    	$hsl = $this->db->query("SELECT 
    C.`id`,
    P.`subcategory_id`,
    C.`code`,
    C.`name`,
    C.`description`,
    IF(C.`image` IS NULL,'no_image.png',C.`image`) AS image,
    V.`name` AS variant, 
    SUM(ROUND(V.`quantity`)) AS qty,
    (ROUND(P.`price`)+ROUND(V.`price`)) AS price
FROM (`sma_categories` C LEFT JOIN `sma_products` P ON P.`subcategory_id` = C.`id`) LEFT JOIN `sma_product_variants` V ON V.`product_id` = P.`id`
WHERE P.`subcategory_id` IS NOT NULL 
AND (V.`quantity` > 0 OR V.`quantity` IS NOT NULL)
AND C.`code`= '".$pcid."'
GROUP BY
    C.`id`,
    P.`subcategory_id`,
    C.`name`,
    C.`image`,
    V.`name`
ORDER BY C.`id` DESC");
        // foreach ($hsl->result() as $key => $value) {
        //     $data = array(
        //         'id'                => $value->id,
        //         'subcategory_id'    => $value->subcategory_id,
        //         'code'              => $value->code,
        //         'name'              => $value->name,
        //         'description'       => $value->description,
        //         'image'             => $value->image,
        //         'variant'           => $value->variant,
        //         'qty'               => $value->qty
        //     );
        //     array_push($arr, $data);
        // }
        	echo json_encode($hsl->result(),JSON_NUMERIC_CHECK);
    }

    function checkid_cat(){
    	$a = $this->input->get('a');
    	$query = $this->db->select('IF(parent_id = 0,concat("`pc_id`=",id),concat("`pc_ps_id`=",parent_id,id)) as id,code')
    				->from('categories')
    				->where('code',$a)
    				->get();
    	if ($query->num_rows()>0) {
    		// echo json_encode($query->result(),JSON_NUMERIC_CHECK);
    		echo json_encode($query->result(),JSON_NUMERIC_CHECK);
    	}else{
    		return false;
    	}
    }

    function example(){
    	$query = $this->db->query("SELECT 
`customer` AS name,
concat('Rp ',REPLACE(FORMAT(`grand_total`,0),',','.')) AS total_harga,
`total_items` AS total_item,
DATE_FORMAT(`date`,'%d-%m-%Y') AS tanggal
FROM `sma_sales` WHERE `payment_status`='pending' AND {$this->db->dbprefix('sales')}.`biller_id` = '".$this->session->userdata("companies_id")."'");
    	$row = $query->row();
    	// $this->datatables_metronic(json_encode($query->result(),JSON_NUMERIC_CHECK));
        // return json_encode($query->result(),JSON_NUMERIC_CHECK);
        return json_encode($query->result());
    }

    function group_image(){
        $query = $this->db->query("SELECT 
C.`code`,
C.`name`,
C.`image`,
C.`description`
FROM `sma_products` P
LEFT JOIN `sma_categories` C ON C.`id` = P.`subcategory_id`
WHERE C.`image` IS NOT NULL AND C.`parent_id` != 0
GROUP BY P.`subcategory_id`
ORDER BY C.`id` DESC
LIMIT 12");
        return $query->result();
    }
}
?>