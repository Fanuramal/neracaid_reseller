<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/lib_mailgun/vendor/autoload.php');
use Mailgun\Mailgun;

class Billers extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if (!$this->Owner && !$this->Admin && !$this->Sales) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('billers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
    }

    function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);
        $this->page_construct('billers/index', $meta, $this->data);
    }

    function getBillers()
    {
        $this->sma->checkPermissions('index');

        $this->load->library('datatables');
        $this->datatables
            ->select("id, company, name, phone, email, city, cf1, cf2, 
    IF(password IS NULL, IF(company = 'Nines Widosari Co.',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-info\">NWC</a></div>',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-danger send-wa\">Tidak Aktif</a></div>'),
        IF(company = 'Nines Widosari Co.',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-info\">NWC</a></div>',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-success\">Aktif</a></div>')
    ) AS stats")
            ->from("companies")
            ->where('group_name', 'biller')
            // ->add_column("Stats","<a></a>","email")
            ->add_column("Actions", "
                <div class=\"text-center\">
                    <a class=\"tip\" title='" . $this->lang->line("edit_biller") . "' href='" . admin_url('billers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> 
                    <a href='#' class='tip po' title='<b>" . $this->lang->line("delete_biller") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('billers/delete/$1') . "'>" . lang('i_m_sure') . "</a> 
                    <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>
                </div>", "id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }
    
    function saldoBillers($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);
        $this->page_construct('billers/saldo_billers', $meta, $this->data);
    }
    function getSaldoBillers(){
        $this->sma->checkPermissions('index');
        $arr1 = array();
        $sql = $this->db->query("SELECT
    `sma_companies`.`name`,
    `sma_companies`.`cf1` AS bank,
    `sma_companies`.`cf2` AS norek,
    FORMAT(SUM(`sma_fee`.`debit`),0) AS debit,
    IF(FORMAT(SUM(`sma_fee`.`kredit`),0) IS NULL,0,FORMAT(SUM(`sma_fee`.`kredit`),0)) AS kredit,
    FORMAT(SUM(`debit`)-IF(SUM(`kredit`) IS NULL,0,SUM(`kredit`)),0) AS saldo,
    `sma_companies`.`id` AS id
FROM `sma_companies`
LEFT JOIN `sma_fee` ON `sma_fee`.`id_biller` = `sma_companies`.`id`
WHERE `sma_companies`.`change_pass` = 1
    AND `sma_companies`.`group_name` = 'biller'
    AND `sma_companies`.`company` = 'Reseller Online Member'
GROUP BY `sma_fee`.`id_biller`");
        $numrows = $sql->num_rows();

        foreach ($sql->result() as $key => $value) {
            if ($value->debit != '') {
            $arr2 = array(
                $value->name,
                $value->bank,
                $value->norek,
                $value->debit,
                $value->kredit,
                $value->saldo,
                $value->id);
            array_push($arr1, $arr2);
            }
        }

        $arr = array(
            'sEcho' => 0,
            'iTotalRecords' => $numrows,
            'iTotalDisplayRecords' => $numrows,
            'aaData' => $arr1,
            'sColumns' => "name,bank,norek,debit,kredit,saldo,id"
        );
echo json_encode($arr);
    }

    public function saldoDetails($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['dtls'] = $this->companies_model->getDetailsSaldoID($id);
        $this->load->view($this->theme . 'billers/saldo_details', $this->data);
    }

    public function addCredit($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['iden'] = $this->companies_model->getNAmeByID($id);
        $this->load->view($this->theme . 'billers/add_credit', $this->data);
    }

    function getAddCredit(){
        $tgl = $this->input->post('datecredit');
        $kredit = $this->input->post('credit');
        $ket = $this->input->post('ketcredit');
        $id = $this->input->post('idcredit');
        $max = $this->input->post('maxcredit');

        $arr = array(
            'id_biller' => $id,
            'tgl' => date("Y-m-d",strtotime($tgl)),
            'kredit' => $kredit,
            'keterangan' => $ket
        );
        
        if ($kredit > $max) {
            $this->session->set_flashdata('alertMax', '<div class="alert alert-warning"> <strong>Maaf</strong>, Kredit yang anda masukkan lebih dari SALDO</div>');
            admin_redirect('reseller/saldoBillers');
        }else{
            if ($this->db->insert('sma_fee',$arr)) {
                admin_redirect('reseller/saldoBillers');
            }else{
                admin_redirect('reseller/saldoBillers');
            }
        }
    }

    function sendpsw($id = null){
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->sendpass($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => "Random Password Berhasil dikirim"));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("biller_x_deleted_have_sales")));
        }
    }

    function add()
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');
        // $password_agen = $this->input->post('password');
        // $password_verify = $this->input->post('very_password');
        if ($this->input->post('cf1') == "Bank lainnya") {
            $df1 = $this->input->post('bank_second');
        }else{
            $df1 = $this->input->post('cf1');
        }
        $email_agen = $this->input->post('email');
        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                // 'username' => $username_agen,
                // 'password' => password_hash($password_agen, PASSWORD_BCRYPT),
                'username' => NULL,
                'password' => NULL,
                'change_pass' => 0,
                'group_name' => 'biller',
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'cf1' => $df1,
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'invoice_footer' => $this->input->post('invoice_footer'),
                'gst_no' => $this->input->post('gst_no'),
            );
        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('billers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addCompany($data)) {

            $mgClient = new Mailgun('4fdce273820843c1356696bd20a27854-c9270c97-5d337f3f');
            $domain = "neraca.id";

            # Make the call to the client.
            $result = $mgClient->sendMessage($domain, array(
                'from'    => 'Neraca.Id <noreply@neraca.id>',
                'to'      => 'You<'.$email_agen.'>',
                'subject' => '[Reseller Online] Pendaftaran sukses. Silakan ubah password',
                'html'    => 'Selamat Anda terdaftar Reseller Online, silakan buat password dengan mengikuti link berikut : <a href="'.site_url('onlinereseller/change_pass?emails='.$this->encryption->encrypt($email_agen)).'">Create Password</a><p>Anda dapat mengecek status invoice, pembayaran, stok barang dan saldo fee marketing Anda melalui portal : https://nineswidosari.com/ayojadi/onlinereseller <p> Kunjungi juga <a href="'.site_url("onlinereseller/faq").'">FAQ</a> agar lebih jelasnya <p>
                <center><label><b> ### PANDUAN ### </b></label></center><br>
                Mulai berjualan klik: https://nineswidosari.com/readystock/ <br>
                Cara berjualan: https://nineswidosari.com/panduan-berjualan-online-nw/'
        ));
            $this->session->set_flashdata('message', $this->lang->line("biller_added"));
            admin_redirect("billers");
        } else {
            $this->data['logos'] = $this->getLogoList();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'billers/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                'group_name' => 'biller',
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'invoice_footer' => $this->input->post('invoice_footer'),
                'gst_no' => $this->input->post('gst_no'),
            );
        } elseif ($this->input->post('edit_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('reseller');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("biller_updated"));
            admin_redirect("reseller");
        } else {
            $this->data['biller'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['logos'] = $this->getLogoList();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'billers/edit', $this->data);
        }
    }


    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteBiller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("biller_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("biller_x_deleted_have_sales")));
        }
    }

    function suggestions($term = NULL, $limit = NULL)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getBillerSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    function getBiller($id = NULL)
    {
        $this->sma->checkPermissions('index');

        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->company)));
    }

    public function getLogoList()
    {
        $this->load->helper('directory');
        $dirname = "assets/uploads/logos";
        $ext = array("jpg", "png", "jpeg", "gif");
        $files = array();
        if ($handle = opendir($dirname)) {
            while (false !== ($file = readdir($handle)))
                for ($i = 0; $i < sizeof($ext); $i++)
                    if (stristr($file, "." . $ext[$i])) //NOT case sensitive: OK with JpeG, JPG, ecc.
                        $files[] = $file;
            closedir($handle);
        }
        sort($files);
        return $files;
    }

    function biller_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteBiller($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('billers_x_deleted_have_sales'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("billers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('billers'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'billers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_biller_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}
