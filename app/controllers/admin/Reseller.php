<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/lib_mailgun/vendor/autoload.php');
use Mailgun\Mailgun;

class Reseller extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer && $this->Sales) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('billers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
    }

    function index($action = NULL)
    {
        // $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);
        $this->page_construct('billers/index', $meta, $this->data);
    }

    function getBillers()
    {
        // $this->sma->checkPermissions('index');

        $this->load->library('datatables');
        $this->datatables
            ->select("id, company, name, phone, email, city, cf1, cf2, 
    IF(password IS NULL, IF(company = 'Nines Widosari Co.',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-info\">NWC</a></div>',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-danger send-wa\" data-toggle=\"modal\" data-target=\"#MyGenerate\">Tidak Aktif</a></div>'),
        IF(company = 'Nines Widosari Co.',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-info\">NWC</a></div>',
        '<div class=\"text-center\"><a href=\"javascript:;\" class=\"btn btn-success\">Aktif</a></div>')
    ) AS stats")
            ->from("companies")
            ->where('group_name', 'biller')
            // ->add_column("Stats","<a></a>","email")
            ->add_column("Actions", "
                <div class=\"text-center\">
                    <a class=\"tip\" title='" . $this->lang->line("edit_biller") . "' href='" . admin_url('reseller/editreseller/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> 
                    <a href='#' class='tip po' title='<b>" . $this->lang->line("delete_biller") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('reseller/deletereseller/$1') . "'>" . lang('i_m_sure') . "</a> 
                    <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>
                </div>", "id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }

    function editreseller($id = NULL)
    {
        // $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                'group_name' => 'biller',
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'invoice_footer' => $this->input->post('invoice_footer'),
                'gst_no' => $this->input->post('gst_no'),
            );
        } elseif ($this->input->post('edit_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('reseller');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("biller_updated"));
            admin_redirect("reseller");
        } else {
            $this->data['biller'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['logos'] = $this->getLogoList();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'billers/edit', $this->data);
        }
    }

    function deletereseller($id = NULL)
    {
        // $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteBiller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("biller_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("biller_x_deleted_have_sales")));
        }
    }

    function add()
    {
        $img_ktp = "";
        // $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');
        // $password_agen = $this->input->post('password');
        // $password_verify = $this->input->post('very_password');
        if ($this->input->post('cf1') == "Bank lainnya") {
            $df1 = $this->input->post('bank_second');
        }else{
            $df1 = $this->input->post('cf1');
        }

        $config['upload_path']          = './assets/uploads/ktp/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['encrypt_name']         = true;
                $config['max_size']             = 5000;
                $config['max_width']            = 2024;
                $config['max_height']           = 2024;
                $this->load->library('upload', $config);
        if ($this->upload->do_upload('ktp_image')) {
            // $img_ktp .= $_FILES['ktp_image']['name'];
            $data = $this->upload->data();
            $img_ktp .= $data['file_name'];
        }

        $email_agen = $this->input->post('email');
        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                // 'username' => $username_agen,
                // 'password' => password_hash($password_agen, PASSWORD_BCRYPT),
                'username' => NULL,
                'password' => NULL,
                'change_pass' => 0,
                'group_name' => 'biller',
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'cf1' => $df1,
                'cf2' => $this->input->post('cf2'),
                'cf3' => $img_ktp,
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'invoice_footer' => $this->input->post('invoice_footer'),
                'gst_no' => $this->input->post('gst_no'),
            );
        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('reseller');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addCompany($data)) {

            $mgClient = new Mailgun('4fdce273820843c1356696bd20a27854-c9270c97-5d337f3f');
            $domain = "neraca.id";

            # Make the call to the client.
            $result = $mgClient->sendMessage($domain, array(
                'from'    => 'Neraca.Id <noreply@neraca.id>',
                'to'      => 'You<'.$email_agen.'>',
                'subject' => '[Reseller Online] Pendaftaran sukses. Silakan ubah password',
                'html'    => 'Selamat Anda terdaftar Reseller Online, silakan buat password dengan mengikuti link berikut : <a href="'.site_url('onlinereseller/change_pass?emails='.$this->encryption->encrypt($email_agen)).'">Create Password</a><p>Anda dapat mengecek status invoice, pembayaran, stok barang dan saldo fee marketing Anda melalui portal : https://nineswidosari.com/ayojadi/onlinereseller <p> Kunjungi juga <a href="'.site_url("onlinereseller/faq").'">FAQ</a> agar lebih jelasnya <p>
                <center><label><b> ### PANDUAN ### </b></label></center><br>
                Mulai berjualan klik: https://nineswidosari.com/readystock/ <br>
                Cara berjualan: https://nineswidosari.com/panduan-berjualan-online-nw/'
        ));
            $this->session->set_flashdata('message', $this->lang->line("biller_added"));
            admin_redirect("reseller");
        } else {
            $this->data['logos'] = $this->getLogoList();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'billers/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        // $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
                $config['upload_path']          = './assets/uploads/ktp/';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['encrypt_name']         = true;
                    $config['max_size']             = 5000;
                    $config['max_width']            = 2024;
                    $config['max_height']           = 2024;
                    $this->load->library('upload', $config);
            if ($this->upload->do_upload('ktp_image')) {
                // $img_ktp .= $_FILES['ktp_image']['name'];
                $dataKTP = $this->upload->data();
                $img_ktp .= $dataKTP['file_name'];
            
                $data = array('name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'group_id' => $this->input->post('gi'),
                    'group_name' => $this->input->post('gn'),
                    'customer_group_name' => $this->input->post('cgn'),
                    'customer_group_id' => $this->input->post('cgi'),
                    'company' => $this->input->post('company'),
                    'address' => $this->input->post('address'),
                    'vat_no' => $this->input->post('vat_no'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'postal_code' => $this->input->post('postal_code'),
                    'country' => $this->input->post('country'),
                    'phone' => $this->input->post('phone'),
                    'logo' => $this->input->post('logo'),
                    'cf1' => $this->input->post('cf1'),
                    'cf2' => $this->input->post('cf2'),
                    'cf3' => $img_ktp,
                    'cf4' => $this->input->post('cf4'),
                    'cf5' => $this->input->post('cf5'),
                    'cf6' => $this->input->post('cf6'),
                    'invoice_footer' => $this->input->post('invoice_footer'),
                    'gst_no' => $this->input->post('gst_no')
                );
            }else{
                $data = array('name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'group_id' => $this->input->post('gi'),
                    'group_name' => $this->input->post('gn'),
                    'customer_group_name' => $this->input->post('cgn'),
                    'customer_group_id' => $this->input->post('cgi'),
                    'company' => $this->input->post('company'),
                    'address' => $this->input->post('address'),
                    'vat_no' => $this->input->post('vat_no'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'postal_code' => $this->input->post('postal_code'),
                    'country' => $this->input->post('country'),
                    'phone' => $this->input->post('phone'),
                    'logo' => $this->input->post('logo'),
                    'cf1' => $this->input->post('cf1'),
                    'cf2' => $this->input->post('cf2'),
                    'cf4' => $this->input->post('cf4'),
                    'cf5' => $this->input->post('cf5'),
                    'cf6' => $this->input->post('cf6'),
                    'invoice_footer' => $this->input->post('invoice_footer'),
                    'gst_no' => $this->input->post('gst_no')
                );
            }
        } elseif ($this->input->post('edit_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('reseller');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("biller_updated"));
            admin_redirect("reseller");
        } else {
            $this->data['biller'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['logos'] = $this->getLogoList();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'billers/edit', $this->data);
        }
    }

    public function getLogoList()
    {
        $this->load->helper('directory');
        $dirname = "assets/uploads/logos";
        $ext = array("jpg", "png", "jpeg", "gif");
        $files = array();
        if ($handle = opendir($dirname)) {
            while (false !== ($file = readdir($handle)))
                for ($i = 0; $i < sizeof($ext); $i++)
                    if (stristr($file, "." . $ext[$i])) //NOT case sensitive: OK with JpeG, JPG, ecc.
                        $files[] = $file;
            closedir($handle);
        }
        sort($files);
        return $files;
    }

    public function saldoBillers($action = NULL)
    {
        // $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);
        $this->page_construct('billers/saldo_billers', $meta, $this->data);
    }
    

    function getActive(){
        // $this->sma->checkPermissions('index');
        $arr1 = array();
        $sql = $this->db->query("SELECT
C.`name` ,
C.`phone`,
S.`date` AS awal,
DATE_ADD(S.`date`, INTERVAL 1 YEAR) AS akhir,
DATEDIFF(DATE_ADD(S.`date`, INTERVAL 1 YEAR),NOW()) AS sisa
FROM `sma_sales` S 
LEFT JOIN `sma_companies` C ON C.`id` = S.`customer_id`
WHERE (S.`payment_status` = 'paid' OR S.`payment_status` = 'partial')
AND S.`paid` >= 3000000
AND C.`customer_group_name` = 'Stokis'
AND C.`group_name` = 'customer'
AND S.`id` IN (SELECT MAX(id) FROM `sma_sales` S WHERE S.`paid` >= 3000000 GROUP BY S.`customer`)
AND C.`different_status` = 1
GROUP BY S.`customer`
ORDER BY `sisa` ASC");
        $numrows = $sql->num_rows();

        foreach ($sql->result() as $key => $value) {
            $arr2 = array(
                $value->name,
                $value->phone,
                $value->awal,
                $value->akhir,
                $value->sisa);
            array_push($arr1, $arr2);
        }

        $arr = array(
            'sEcho' => 0,
            'iTotalRecords' => $numrows,
            'iTotalDisplayRecords' => $numrows,
            'aaData' => $arr1,
            'sColumns' => "name,phone,awal,akhir,sisa"
        );
echo json_encode($arr);
    }
    
    function getActiveOld(){
        // $this->sma->checkPermissions('index');
        $arr1 = array();
        $sql = $this->db->query("SELECT
C.`name` ,
C.`phone`,
S.`date` AS awal,
DATE_ADD(S.`date`, INTERVAL 1 YEAR) AS akhir,
DATEDIFF(DATE_ADD(S.`date`, INTERVAL 1 YEAR),NOW()) AS sisa
FROM `sma_sales` S 
LEFT JOIN `sma_companies` C ON C.`id` = S.`customer_id`
WHERE (S.`payment_status` = 'paid' OR S.`payment_status` = 'partial')
AND S.`paid` >= 1000000
AND C.`customer_group_name` = 'Stokis'
AND C.`group_name` = 'customer'
AND S.`id` IN (SELECT MAX(id) FROM `sma_sales` S WHERE S.`paid` >= 1000000 GROUP BY S.`customer`)
AND (C.`different_status` = 0 OR C.`different_status` IS NULL)
GROUP BY S.`customer`
ORDER BY `sisa` ASC");
        $numrows = $sql->num_rows();

        foreach ($sql->result() as $key => $value) {
            $arr2 = array(
                $value->name,
                $value->phone,
                $value->awal,
                $value->akhir,
                $value->sisa);
            array_push($arr1, $arr2);
        }

        $arr = array(
            'sEcho' => 0,
            'iTotalRecords' => $numrows,
            'iTotalDisplayRecords' => $numrows,
            'aaData' => $arr1,
            'sColumns' => "name,phone,awal,akhir,sisa"
        );
echo json_encode($arr);
    }
    public function active(){
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers_active')));
            $meta = array('page_title' => lang('customers'), 'bc' => $bc);
            $this->page_construct('customers/active', $meta, $this->data);   
        }

    function getSaldoBillers(){
        // $this->sma->checkPermissions('index');
        $arr1 = array();
        $sql = $this->db->query("SELECT
    `sma_companies`.`name`,
    `sma_companies`.`cf1` AS bank,
    `sma_companies`.`cf2` AS norek,
    `sma_companies`.`cf4` AS atas_nama,
    FORMAT(SUM(IF(`sma_fee`.`debit` IS NULL,0,`sma_fee`.`debit`)),0) AS debit,
    IF(FORMAT(SUM(`sma_fee`.`kredit`),0) IS NULL,0,FORMAT(SUM(`sma_fee`.`kredit`),0)) AS kredit,
    FORMAT(SUM(IF(`debit` IS NULL,0,`debit`))-SUM(IF(`kredit` IS NULL,0,`kredit`)),0) AS saldo,
    `sma_companies`.`id` AS id
FROM `sma_companies`
LEFT JOIN `sma_fee` ON `sma_fee`.`id_biller` = `sma_companies`.`id`
WHERE `sma_companies`.`change_pass` = 1
    AND `sma_companies`.`group_name` = 'biller'
    AND `sma_companies`.`company` = 'Reseller Online Member'
GROUP BY `sma_fee`.`id_biller`");
        $numrows = $sql->num_rows();

        foreach ($sql->result() as $key => $value) {
            if ($value->debit != 0 && $value->saldo != 0) {
            $arr2 = array(
                $value->name,
                $value->bank,
                $value->norek,
                $value->atas_nama,
                $value->debit,
                $value->kredit,
                $value->saldo,
                $value->id);
            array_push($arr1, $arr2);
            }
        }

        $arr = array(
            'sEcho' => 0,
            'iTotalRecords' => $numrows,
            'iTotalDisplayRecords' => $numrows,
            'aaData' => $arr1,
            'sColumns' => "name,bank,norek,atas_nama,debit,kredit,saldo,id"
        );
echo json_encode($arr);
    }

    public function saldoDetails($id = null)
    {
        // $this->sma->checkPermissions(false, true);
        $this->data['dtls'] = $this->companies_model->getDetailsSaldoID($id);
        $this->load->view($this->theme . 'billers/saldo_details', $this->data);
    }

    public function addCredit($id = null)
    {
        // $this->sma->checkPermissions(false, true);
        $this->data['iden'] = $this->companies_model->getNAmeByID($id);
        $this->load->view($this->theme . 'billers/add_credit', $this->data);
    }

    function getAddCredit(){
        $tgl = $this->input->post('datecredit');
        $kredit = $this->input->post('credit');
        $ket = $this->input->post('ketcredit');
        $id = $this->input->post('idcredit');
        $max = $this->input->post('maxcredit');

        $arr = array(
            'id_biller' => $id,
            'tgl' => date("Y-m-d",strtotime($tgl)),
            'kredit' => $kredit,
            'keterangan' => $ket
        );
        
        if ($kredit > $max) {
            $this->session->set_flashdata('alertMax', '<div class="alert alert-warning"> <strong>Maaf</strong>, Kredit yang anda masukkan lebih dari SALDO</div>');
            admin_redirect('reseller/saldoBillers');
        }else{
            if ($this->db->insert('sma_fee',$arr)) {
                admin_redirect('reseller/saldoBillers');
            }else{
                admin_redirect('reseller/saldoBillers');
            }
        }
    }
    function listBalance(){
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('commission_payment')));
        $meta = array('page_title' => lang('commission_payment'), 'bc' => $bc);
        $this->page_construct('billers/commission_payment', $meta, $this->data);
    }
    function getBalance($pdf = NULL, $xls = NULL){
        // $this->sma->checkPermissions('index');

        if ($pdf || $xls) {
        $v = "";
        $sd = $this->input->get('sd');
        $ed = $this->input->get('ed');
        if ($sd != '' && $ed != '') {
            $v .= "AND `sma_fee`.`tgl` >= DATE('".date('Y-m-d',strtotime($sd))."') AND `sma_fee`.`tgl` <= DATE('".date('Y-m-d',strtotime($ed))."')";
        }
        elseif ($ed != '') {
            $v .= "AND `sma_fee`.`tgl` <= DATE('".date('Y-m-d',strtotime($ed))."')";
        }
        elseif ($sd != '') {
            $v .= "AND `sma_fee`.`tgl` >= DATE('".date('Y-m-d',strtotime($sd))."')";
        }

        $query = $this->db->query("SELECT
    `sma_fee`.`id`,
    `sma_fee`.`tgl` AS tanggal,
    `sma_companies`.`name` AS name,
    `sma_companies`.`cf2` AS norek,
    `sma_companies`.`cf1` AS bank,
    `sma_companies`.`cf4` AS attn,
    ROUND(IF(`sma_fee`.`kredit` IS NULL,0,`sma_fee`.`kredit`)) AS kredit,
    `sma_fee`.`keterangan`
FROM `sma_fee`
LEFT JOIN `sma_companies` ON `sma_companies`.`id` = `sma_fee`.`id_biller`
LEFT JOIN `sma_sales` ON `sma_sales`.`id` = `sma_fee`.`id_sale`
WHERE `sma_companies`.`change_pass` = 1
    AND `sma_companies`.`group_name` = 'biller'
    AND `sma_companies`.`company` = 'Reseller Online Member'
    AND kredit > 0
    ".$v."
ORDER BY `sma_fee`.`tgl` DESC");
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);

                $this->excel->getActiveSheet()->setTitle(lang('fee_payment_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('bcf2'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('bcf1'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('bcf4'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('kredit'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('descriptions'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->tanggal);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->norek);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->bank);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->attn);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->kredit);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->keterangan);
                    $row++;
                }
                
                $this->excel->getActiveSheet()->mergeCells('A'.($row+1).':E'.($row+1));
                $this->excel->getActiveSheet()->SetCellValue('A'.($row+1), 'Total');
                $this->excel->getActiveSheet()->SetCellValue('F'.($row+1), '=SUM(F2:F'.$row.')');

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $filename = 'fee_payment_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else{
            $v = "";
        $sd = $this->input->get('sd');
        $ed = $this->input->get('ed');
        if ($sd != '' && $ed != '') {
            $v .= "AND `sma_fee`.`tgl` >= DATE('".date('Y-m-d',strtotime($sd))."') AND `sma_fee`.`tgl` <= DATE('".date('Y-m-d',strtotime($ed))."')";
        }
        elseif ($ed != '') {
            $v .= "AND `sma_fee`.`tgl` <= DATE('".date('Y-m-d',strtotime($ed))."')";
        }
        elseif ($sd != '') {
            $v .= "AND `sma_fee`.`tgl` >= DATE('".date('Y-m-d',strtotime($sd))."')";
        }

        $arr1 = array();
        $sql = $this->db->query("SELECT
    `sma_fee`.`id`,
    `sma_fee`.`tgl` AS tanggal,
    `sma_companies`.`name` AS name,
    `sma_companies`.`cf2` AS norek,
    `sma_companies`.`cf1` AS bank,
    `sma_companies`.`cf4` AS attn,
    ROUND(IF(`sma_fee`.`kredit` IS NULL,0,`sma_fee`.`kredit`)) AS kredit,
    `sma_fee`.`keterangan`
FROM `sma_fee`
LEFT JOIN `sma_companies` ON `sma_companies`.`id` = `sma_fee`.`id_biller`
LEFT JOIN `sma_sales` ON `sma_sales`.`id` = `sma_fee`.`id_sale`
WHERE `sma_companies`.`change_pass` = 1
    AND `sma_companies`.`group_name` = 'biller'
    AND `sma_companies`.`company` = 'Reseller Online Member'
    AND kredit > 0
    ".$v."
ORDER BY `sma_fee`.`tgl` DESC");
        $numrows = $sql->num_rows();

        foreach ($sql->result() as $key => $value) {
            $arr2 = array(
                date('d-m-Y',strtotime($value->tanggal)),
                $value->name,
                $value->norek,
                $value->bank,
                $value->attn,
                $value->kredit,
                $value->keterangan,
                $value->id);
            array_push($arr1, $arr2);
        }

        $arr = array(
            'sEcho' => 0,
            'iTotalRecords' => $numrows,
            'iTotalDisplayRecords' => $numrows,
            'aaData' => $arr1,
            'sColumns' => "tanggal,name,norek,bank,attn,kredit,keterangan,id"
        );
echo json_encode($arr);
        }
    }

    function deleteCredit($id = null){
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteCredit($id)) {
            $this->session->set_flashdata('message', lang('credit_deleted'));
            admin_redirect('reseller/listBalance');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">'.lang('credit_x_deleted').'</div>');
            admin_redirect('reseller/listBalance');
        }
    }

    function editCredit($id = null){

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }           
            $this->data['getCredit'] = $this->companies_model->getCreditById($id);
            $this->load->view($this->theme . 'billers/edit_credit', $this->data);
    }

    function getUpdateCredit(){
        $id = $this->input->post('idcredit');
        $datecredit = $this->input->post('datecredit');
        $credit = $this->input->post('credit');
        $ketcredit = $this->input->post('ketcredit');
        $maxcredit = $this->input->post('maxcredit');

        $data = array(
            'tgl' => date('Y-m-d',strtotime($datecredit)),
            'kredit' => $credit,
            'keterangan' => $ketcredit
        );
        if ($credit <= $maxcredit) {
            $this->db->where('id', $id);
            $this->db->update('fee', $data);
            $this->session->set_flashdata('message', lang('credit_updated'));
            admin_redirect('reseller/listBalance');
        }else{
            $this->session->set_flashdata('error', lang('credit_x_updated'));
            admin_redirect('reseller/listBalance');
        }
    }
function biller_actions()
    {
        // if (!$this->Owner && !$this->GP['bulk_actions']) {
        //     $this->session->set_flashdata('warning', lang('access_denied'));
        //     redirect($_SERVER["HTTP_REFERER"]);
        // }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    // $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteBiller($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('billers_x_deleted_have_sales'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("billers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('billers'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'billers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_biller_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function generateE(){
        $a = $this->input->post('namaP');
        $b = $this->input->post('teleponP');
        $c = $this->input->post('emailP');
        $cc = $this->encryption->encrypt($c);
        $ccc = str_replace('+', 'PLUS', $cc);
        $d = "Assalamualaikum+".$a."%2C+anda+belum+mengaktifkan+Password%2C+silakan+kunjungi+link+ini+untuk+mengaktifkannya+https%3A%2F%2Fnineswidosari.com%2Fayojadi%2Fonlinereseller%2Fchange_pass%3Femails%3D".$ccc;
        // $d = "Assalamualaikum+".$a."%2C+anda+belum+mengaktifkan+Password%2C+silakan+kunjungi+link+ini+untuk+mengaktifkannya+http%3A%2F%2Flocalhost%2Fneracaid_reseller%2Fonlinereseller%2Fchange_pass%3Femails%3D".$ccc;
        $msg = '<script>window.open("https://wa.me/'.$b.'?text='.$d.'", "_self");</script>';
        echo $msg;
        // echo $d;
    }
}