<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Purchases
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['add_purchase']                       = "Tambahkan Pembelian";
$lang['edit_purchase']                      = "Edit Pembelian";
$lang['delete_purchase']                    = "Hapus Pembelian";
$lang['delete_purchases']                   = "Hapus Pembelian";
$lang['purchase_added']                     = "Pembelian berhasil ditambahkan";
$lang['purchase_updated']                   = "Pembelian berhasil diperbarui";
$lang['purchase_deleted']                   = "Pembelian berhasil dihapus";
$lang['purchases_deleted']                  = "Pembelian berhasil dihapus";
$lang['ref_no']                             = "nomor referensi";
$lang['purchase_details']                   = "Detail Pembelian";
$lang['email_purchase']                     = "Pembelian melalui Email";
$lang['purchase_quantity']                  = "Kuantitas Pembelian";
$lang['please_select_warehouse']            = "Silakan pilih gudang";
$lang['purchase_by_csv']                    = "Tambahkan Pembelian oleh CSV";
$lang['received']                           = "Diterima";
$lang['more_options']                       = "More Options";
$lang['add_standard_product']               = "Tambahkan Produk Standar";
$lang['product_code_is_required']           = "Kode Produk diperlukan";
$lang['product_name_is_required']           = "Nama Produk wajib diisi";
$lang['product_category_is_required']       = "Kategori Produk wajib diisi";
$lang['product_unit_is_required']           = "Unit Produk diperlukan";
$lang['product_cost_is_required']           = "Biaya Produk diperlukan";
$lang['product_price_is_required']          = "Harga Produk diperlukan";
$lang['ordered']                            = "Dipesan";
$lang['tax_rate_name']                      = "Nama Tarif Pajak";
$lang['no_purchase_selected']               = "Tidak ada pembelian yang dipilih. Silakan pilih setidaknya satu pembelian.";
$lang['view_payments']                      = "Lihat Pembayaran";
$lang['add_payment']                        = "Tambahkan Pembayaran";
$lang['payment_reference_no']               = "Referensi Pembayaran No";
$lang['edit_payment']                       = "Edit Pembayaran";
$lang['delete_payment']                     = "Hapus Pembayaran";
$lang['delete_payments']                    = "Hapus Pembayaran";
$lang['payment_added']                      = "Pembayaran berhasil ditambahkan";
$lang['payment_updated']                    = "Pembayaran berhasil diperbarui";
$lang['payment_deleted']                    = "Pembayaran berhasil dihapus";
$lang['payments_deleted']                   = "Pembayaran berhasil dihapus";
$lang['paid_by']                            = "Dibayar oleh";
$lang['payment_reference']                  = "Referensi pembayaran";
$lang['view_purchase_details']              = "Lihat Detail Pembelian";
$lang['purchase_no']                        = "Nomor pembelian";
$lang['balance']                            = "Balance";
$lang['product_option']                     = "Opsi Produk";
$lang['payment_sent']                       = "Pembayaran terkirim";
$lang['payment_note']                       = "Catatan Pembayaran";
$lang['payment_received']                   = "Pembayaran diterima";
$lang['purchase_status']                    = "Status Pembelian";
$lang['purchase_x_edited_older_than_x_days']= "Pembelian tidak dapat diedit karena pembelian ini lebih tua dari %d hari.";
$lang['pr_not_found']                       = "Tidak ada produk yang ditemukan";
$lang['line_no']                            = "Nomor Baris";
$lang['expense']                            = "Biaya";
$lang['edit_expense']                       = "Edit Biaya";
$lang['delete_expense']                     = "Hapus Biaya";
$lang['delete_expenses']                    = "Hapus Biaya";
$lang['expense_added']                      = "Biaya berhasil ditambahkan";
$lang['expense_updated']                    = "Biaya berhasil diperbarui";
$lang['expense_deleted']                    = "Biaya berhasil dihapus";
$lang['reference']                          = "Referensi";
$lang['expenses_deleted']                   = "Pengeluaran berhasil dihapus";
$lang['expense_note']                       = "Catatan Biaya";
$lang['no_expense_selected']                = "Tidak ada biaya yang dipilih. Silakan pilih setidaknya satu biaya.";
$lang['please_select_supplier']             = "Silakan pilih pemasok";
$lang['unit_cost']                          = "Biaya Unit";
$lang['product_expiry_date_issue']          = "Tanggal kedaluwarsa produk bermasalah";
$lang['received_more_than_ordered']         = "Kuantitas yang diterima lebih dari yang dipesan";
$lang['purchase_order']                     = "Pesanan pembelian";
$lang['payment_term']                       = "Jangka waktu pembayaran";
$lang['payment_term_tip']                   = "Harap ketik jumlah hari saja (bilangan bulat) saja";
$lang['due_on']                             = "Jatuh tempo pada";
$lang['paid_amount']                        = "Jumlah pembayaran";
$lang['return_purchase']                    = "Pembelian Kembali";
$lang['return_surcharge']                   = "Pengembalian Biaya";
$lang['return_amount']                      = "Jumlah Pengembalian";
$lang['purchase_reference']                 = "Referensi Pembelian";
$lang['return_purchase_no']                 = "Pembelian Kembali No";
$lang['view_return']                        = "Lihat Kembali";
$lang['return_purchase_deleted']            = "Pengembalian pembelian berhasil dihapus";
$lang['purchase_status_x_received']         = "Status pembelian tidak diterima";
$lang['total_before_return']                = "Total Sebelum Kembali";
$lang['return_amount']                      = "Jumlah Pengembalian";
$lang['return_items']                       = "Mengembalikan item";
$lang['surcharge']                          = "Biaya tambahan";
$lang['returned_items']                     = "Item yang dikembalikan";
$lang['return_quantity']                    = "Kuantitas yang dikembalikan";
$lang['seller']                             = "Penjual";
$lang['users']                              = "Pengguna";
$lang['return_note']                        = "Return Note";
$lang['return_purchase_added']              = "Pengembalian pembelian berhasil ditambahkan";
$lang['return_has_been_added']              = "Beberapa item telah dikembalikan untuk pembelian ini";
$lang['return_tip']                         = "Harap edit jumlah pengembalian di bawah. Anda dapat menghapus item atau mengatur jumlah pengembalian ke nol jika tidak dikembalikan";
$lang['return_surcharge']                   = "Pengembalian Biaya";
$lang['payment_returned']                   = "Pembayaran Dikembalikan";
$lang['payment_note']                       = "Catatan Pembayaran";
$lang['payment_status']                     = "Status pembayaran";
$lang['view_return_details']                = "Lihat Detail Pengembalian";
$lang['adjust_payments']                    = "Harap sesuaikan pembayaran untuk pembelian secara manual";
$lang['purchase_x_action']                  = "Tindakan ini tidak dapat dilakukan untuk pembelian dengan catatan pengembalian";
$lang['purchase_already_returned']          = "Pembelian sudah memiliki catatan pengembalian";
$lang['purchase_is_returned']               = "Pembelian memiliki catatan pengembalian";
$lang['calculate_unit_cost']                = "Hitung Biaya Satuan";
$lang['purchase_already_paid']              = "Status pembayaran sudah dibayar untuk pembelian";
$lang['payment_sent']                       = "Pembayaran terkirim";
$lang['update_supplier_email']              = "Harap perbarui alamat email pemasok";
