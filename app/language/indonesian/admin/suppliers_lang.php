<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Suppliers
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['add_supplier']                       = "Tambahkan Pemasok";
$lang['edit_supplier']                      = "Edit Supplier";
$lang['delete_supplier']                    = "Hapus Supplier";
$lang['delete_suppliers']                   = "Hapus Suppliers";
$lang['supplier_added']                     = "Supplier Berhasil dibuat";
$lang['suppliers_added']                    = "Supplier Berhasil dibuat";
$lang['supplier_updated']                   = "Supplier Berhasil diupdated";
$lang['supplier_deleted']                   = "Supplier Berhasil dihapus";
$lang['suppliers_deleted']                  = "Supplier Berhasil dihapus";
$lang['import_by_csv']                      = "Tambah / Perbarui Pemasok oleh CSV";
$lang['edit_profile']                       = "Edit User";
$lang['delete_user']                        = "Hapus User";
$lang['no_supplier_selected']               = "Tidak ada supplier yang dipilih. Silakan pilih setidaknya satu supplier.";
$lang['pw_not_same']                        = "Konfirmasi kata sandi tidak cocok dengan kata sandi Anda";
$lang['user_added']                         = "Pengguna supplier berhasil ditambahkan";
$lang['user_deleted']                       = "Pengguna supplier berhasil dihapus";
$lang['supplier_x_deleted_have_purchases']  = "Aksi: Gagal! supplier melakukan pembelian";
$lang['suppliers_x_deleted_have_purchases'] = "Beberapa pemasok tidak dapat dihapus karena ada pembelian";
$lang['check_supplier_email']               = "Silakan periksa email supplier";
$lang['supplier_already_exist']             = "Supplier sudah ada dengan alamat email yang sama";
$lang['line_no']                            = "Line Number";
$lang['first_6_required']                   = "Enam (6) kolom pertama diperlukan dan yang lainnya opsional.";
$lang['are_required']                       = "diperlukan";
$lang['data_x_suppliers']                   = "Data CSV bukan milik supplier mana pun";
$lang['csv_update_tip']                     = "Sistem akan memeriksa apakah email tersebut milik supplier mana pun kemudian akan memperbarui bahwa supplier itu sebaliknya akan menambah supplier baru. Jika alamat email sudah ada untuk penagih / pelanggan itu akan diabaikan.";
