<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Cron Job
 * 
 * Last edited:
 * 12th August 2016
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * The %d, %s will replaced. You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['cron_job']                       = "CRON JOB";
$lang['order_ref_updated']              = "Nomor referensi pesanan telah diatur ulang.";
$lang['x_pending_to_due']               = "% d status tertunda faktur telah diubah menjadi jatuh tempo.";
$lang['x_partial_to_due']               = "% d Status faktur yang dibayar sebagian telah berubah menjadi jatuh tempo.";
$lang['x_purchases_changed']            = "% d pembelian tertunda / sebagian dibayar telah jatuh tempo.";
$lang['x_products_expired']             = "% d produk dengan jumlah total% d telah kedaluwarsa.";
$lang['x_promotions_expired']           = "% d promosi produk kedaluwarsa.";
$lang['user_login_deleted']             = "Catatan login pengguna sebelum% s telah dihapus.";
$lang['backup_done']                    = "Cadangan basis data berhasil dan cadangan yang lebih lama dari 30 hari dihapus.";
$lang['update_available']               = "Pembaruan baru tersedia, silakan kunjungi menu pembaruan di bawah pengaturan.";
$lang['all_warehouses']                 = "Semua Gudang";
$lang['products_sale']                  = "Pendapatan Produk ";
$lang['order_discount']                 = "Diskon Pemesanan";
$lang['products_cost']                  = "Biaya Produk";
$lang['expenses']                       = "Beban";
$lang['profit']                         = "Keuntungan";
$lang['total_sales']                    = "Total Penjualan";
$lang['return_sales']                   = "Pengembalian Penjualan";
$lang['total_purchases']                = "Total Pembelian";
$lang['general_ledger']                 = "Jurnal umum";
$lang['received']                       = "Diterima";
$lang['paid']                           = "Dibayar";
$lang['taxes']                          = "Pajak";
