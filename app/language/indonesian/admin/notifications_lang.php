<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Notifications
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */

$lang['notification']               = "Notifikasi";
$lang['add_notification']           = "Buat Notifikasi";
$lang['edit_notification']          = "Edit Notifikasi";
$lang['delete_notification']        = "Hapus Notifikasi";
$lang['delete_notifications']       = "Hapus Notifikasi";
$lang['notification_added']         = "Notifikasi berhasil dibuat";
$lang['notification_updated']       = "Notifikasi berhasil diupdate";
$lang['notification_deleted']       = "Notifikasi berhasil dihapus";
$lang['notifications_deleted']      = "Notifikasi berhasil dihapus";
$lang['submitted_at']               = "Submitted at";
$lang['till']                       = "Till";
$lang['comment']                    = "koment";
$lang['for_customers_only']         = "Hanya untuk Customers";
$lang['for_staff_only']             = "Hanya untuk Staff";
$lang['for_both']                   = "Untuk keduanya";
$lang['till']                       = "Till";