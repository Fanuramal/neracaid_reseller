<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Front End Settings
 *
 * Last edited:
 * 21st March 2017
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to support@tecdiary.com
 * Thank you
 */


$lang['shop_name']                      = "Nama";
$lang['page']                           = "Halaman";
$lang['pages']                          = "Halaman";
$lang['slug']                           = "Slug";
$lang['order']                          = "Order";
$lang['menu_order']                     = "Menu Order";
$lang['body']                           = "Body";
$lang['edit_page']                      = "Edit Halaman";
$lang['delete_page']                    = "Hapus Halaman";
$lang['delete_pages']                   = "Hapus Halaman";
$lang['page_added']                     = "Halaman berhasil dibuat";
$lang['page_updated']                   = "Halaman berhasil diupdate";
$lang['page_deleted']                   = "Halaman berhasil dihapus";
$lang['pages_deleted']                  = "Halaman berhasil dihapus";
$lang['order_no']                       = "Order No.";
$lang['title_required']                 = "Judul dibutuhkan";
$lang['slug_required']                  = "Slug dibutuhkan";
$lang['description_required']           = "Deskripsi dibutuhkan";
$lang['body_required']                  = "Body dibutuhkan";
$lang['about_link']                     = "Tentang halaman";
$lang['terms_link']                     = "Ketentuan halaman";
$lang['privacy_link']                   = "Privacy Policy Page";
$lang['contact_link']                   = "halaman kontak";
$lang['cookie_link']                    = "Cookie Page";
$lang['cookie_message']                 = "Cookie Message";
$lang['payment_text']                   = "Teks pembayaran";
$lang['follow_text']                    = "Follow Text";
$lang['facebook']                       = "Facebook";
$lang['twitter']                        = "Twitter";
$lang['google_plus']                    = "Google Plus";
$lang['instagram']                      = "Instgram";
$lang['image']                          = "Image";
$lang['link']                           = "Link";
$lang['caption']                        = "Caption";
$lang['show_in_top_menu']               = "Show in top menus";
$lang['products_page']                  = "Products page (grid style)";
$lang['leave_gap']                      = "Leave gap below";
$lang['re_arrange']                     = "Re-arrange products";

