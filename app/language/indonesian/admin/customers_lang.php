<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Customers
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['add_customer']                   = "Tambahkan Pelanggan";
$lang['edit_customer']                  = "Edit Pelanggan ";
$lang['delete_customer']                = "Hapus Pelanggan";
$lang['delete_customers']               = "Hapus Pelanggan";
$lang['customer_added']                 = "Pelanggan berhasil ditambahkan";
$lang['customers_added']                = "Pelanggan berhasil ditambahkan";
$lang['customer_updated']               = "Pelanggan berhasil diperbarui";
$lang['customer_deleted']               = "Pelanggan berhasil dihapus";
$lang['customers_deleted']              = "Pelanggan berhasil dihapus";
$lang['import_by_csv']                  = "Tambah / Perbarui Pelanggan dengan CSV ";
$lang['edit_profile']                   = "Edit Pengguna";
$lang['delete_user']                    = "Hapus pengguna";
$lang['no_customer_selected']           = "Tidak ada pelanggan yang dipilih. Silakan pilih setidaknya satu pelanggan.";
$lang['pw_not_same']                    = "Konfirmasi kata sandi tidak cocok dengan kata sandi Anda";
$lang['user_added']                     = "Pengguna pelanggan berhasil ditambahkan";
$lang['user_deleted']                   = "Pengguna pelanggan berhasil dihapus";
$lang['customer_x_deleted']             = "Aksi: Gagal! pelanggan ini tidak dapat dihapus";
$lang['customer_x_deleted_have_sales']  = "Aksi: Gagal! pelanggan memiliki penjualan";
$lang['customers_x_deleted_have_sales'] = "Beberapa pelanggan tidak dapat dihapus karena mereka memiliki penjualan";
$lang['check_customer_email']           = "Silakan periksa email pelanggan";
$lang['customer_already_exist']         = "Pelanggan sudah ada dengan alamat email yang sama";
$lang['line_no']                        = "Nomor Baris";
$lang['first_6_required']               = "Enam (6) kolom pertama diperlukan dan yang lainnya opsional.";
$lang['paid_by']                        = "Dibayar oleh";
$lang['deposits']                       = "Setoran";
$lang['list_deposits']                  = "Daftar Deposit";
$lang['add_deposit']                    = "Tambahkan Deposit";
$lang['edit_deposit']                   = "Edit Setoran";
$lang['delete_deposit']                 = "Hapus Setoran";
$lang['deposit_added']                  = "Setoran berhasil ditambahkan";
$lang['deposit_updated']                = "Setoran berhasil diperbarui";
$lang['deposit_deleted']                = "Setoran berhasil dihapus";
$lang['deposits_subheading']            = "Silakan gunakan tabel di bawah ini untuk menavigasi atau mencari hasilnya.";
$lang['deposit_note']                   = "Catatan Setoran";
$lang['list_addresses']                 = "Daftar Alamat";
$lang['addresses']                      = "Alamat";
$lang['line1']                          = "Baris Alamat 1";
$lang['line2']                          = "Baris Alamat 2";
$lang['add_address']                    = "Tambahkan alamat";
$lang['edit_address']                   = "Edit Alamat";
$lang['delete_address']                 = "Hapus Alamat";
$lang['address_added']                  = "Alamat berhasil ditambahkan";
$lang['address_updated']                = "Alamat berhasil diperbarui";
$lang['address_deleted']                = "Alamat berhasil dihapus";
$lang['are_required']                   = "diperlukan";
$lang['data_x_customers']               = "Data CSV bukan milik pelanggan mana pun";
$lang['csv_update_tip']                 = "Sistem akan memeriksa apakah email tersebut milik pelanggan mana pun kemudian akan memperbarui bahwa pelanggan sebaliknya akan menambah pelanggan baru. Jika alamat email sudah ada untuk penagih / pemasok, ini akan diabaikan.";
