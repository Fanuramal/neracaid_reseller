<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Apis
 * 
 * Last edited:
 * 28th March 2017
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['api_key']                        = "kunci API";
$lang['api_keys']                       = "Kunci API";
$lang['create_api_key']                 = "Buat API Key";
$lang['delete_api_keys']                = "Hapus API Key";
$lang['key']                            = "Kunci";
$lang['level']                          = "Tingkat";
$lang['ignore_limit']                   = "Abaikan Batas";
$lang['ip_addresses']                   = "Alamat IP";
$lang['api_key_deleted']                = "API Key berhasil dihapus";
$lang['delete_failed']                  = "API key gagal dihapus.";
$lang['api_key_added']                  = "API Key berhasil dibuat";
$lang['reference_required']             = "Referensi diperlukan";
$lang['read']                           = "Baca saja";
$lang['read_write_delete']              = "Baca, Tulis & Hapus";
$lang['ignore_limits']                  = "Abaikan Batas";
